#reload countrycolors

GER = {
	color = RGB { 128 128 128 }
	color_ui = rgb { 128 128 128 }
}
ENG = {
	color = rgb { 191  93  79 }
	color_ui = rgb { 191  93  79 }
}
QUE = {
	color = rgb { 15  19  94 }
	color_ui = rgb { 15  19  94 }
}
SOV = {
	color = rgb { 30  127  46 }
	color_ui = rgb { 0  68  255 }
}
SWE = {
	color = rgb { 57  113  228 }
	color_ui = rgb { 57  113  228 }
}
FRA = {
	color = rgb { 110  142  190 }
	color_ui = rgb { 110  142  190 }
}
LUX = {
	color = rgb { 65 175 179 }
	color_ui = rgb { 85 228 233 }
}
CSA = {
	color = rgb { 145 163 176  }
	color_ui = rgb { 186 205 219 }
}
BEL = {
		color = rgb { 220  216  101 }
		color_ui = rgb { 220  216  101 }
	  }
HOL = {	color = rgb { 203 138 74 }
		color_ui = rgb { 255 179 96 }
}
CZE = {	color = rgb { 76  97  121 }
		color_ui = rgb { 76  97  121 }
}
POL = {
	color = rgb { 190  110  101 }
	color_ui = rgb { 190  110  101 }
}
AUS = {
	color = rgb { 124  78  75 }
	color_ui = rgb { 124  78  75 }
}
LIT = {
	color = rgb { 219 219 119 }
	color_ui = rgb { 255 255 155 }
}
EST = {
	color = rgb { 76  157  195 }
	color_ui = rgb { 76  157  195 }
}
LAT = {
	color = rgb { 112  90  87 }
	color_ui = rgb { 112  90  87 }
}
SPR = {
	color = rgb { 242 205 94 }
	color_ui = rgb { 255 255 122 }
}
ITA = {
	color = rgb { 67 127 63 }
	color_ui = rgb { 87 165 82 }
}
ROM = {
	color = rgb { 235  216  92 }
	color_ui = rgb { 235  216  92 }
}
YUG = {
	color = rgb { 72 73 126 }
	color_ui = rgb { 94 95 164 }
}
SWI = {
	color = rgb { 92  50  34 }
	color_ui = rgb { 92  50  34 }
}
TUR = {
	color = rgb { 171  190  152 }
	color_ui = rgb { 171  190  152 }
}
GRE = {
	color = rgb { 93 181 227 }
	color_ui = rgb { 121 235 255 }
}
ALB = {
	color = rgb { 152  130  191 }
	color_ui = rgb { 152  130  191 }
}
NOR = {
	color = rgb { 111 71 71 }
	color_ui = rgb { 144 92 92 }
}
DEN = {
	color = rgb { 153 116 93 }
	color_ui = rgb { 199 151 121 }
}
BUL = {
	color = rgb { 66  113  69 }
	color_ui = rgb { 66  113  69 }
}
POR = {
	color = rgb { 39 116 70 }
	color_ui = rgb { 51 151 91 }
}
FIN = {
	color = rgb { 205  212  228 }
	color_ui = rgb { 205  212  228 }
}
IRE = {
	color = rgb { 80 159 90 }
	color_ui = rgb { 104 207 117 }
}
HUN = {
	color = rgb { 138  163  117 }
	color_ui = rgb { 138  163  117 }
}
AFG = {
	color = rgb { 64 160 167 }
	color_ui = rgb { 83 208 217 }
}
ARG = {
	color = rgb { 32 41 105 }
	color_ui = rgb { 32 41 105 }
}
AST = {
	color = rgb { 57 143 97 }
	color_ui = rgb { 57  143  97 }
}
BHU = {
	color = rgb { 225  131  148 }
	color_ui = rgb { 225  131  148 }
}
BOL = {
	color = hsv { 255  180  136 }
	color_ui = rgb { 255  180  136 }
}
BRA = {
	color = rgb { 76 145 63 }
	color_ui = rgb { 76  145  63 }
}
CAN = {
	color = rgb { 117 40 40 }
	color_ui = rgb { 117 40 40 }
}
CHI = {
	color = hsv { 0.18 0.6 0.9 } #255 244 190
	color_ui = hsv { 0.18 0.3 0.9 }
}
CHL = {
	color = rgb { 155 101 107 }
	color_ui = rgb { 202 131 139 }
}
COL = {
	color = rgb { 222 187 91 }
	color_ui = rgb { 255 243 118 }
}
COS = {
	color = rgb { 152 128 43 }
	color_ui = rgb { 152 128 43 }
}
ECU = {
	color = rgb { 249 146 98 }
	color_ui = rgb { 255 190 127 }
}
ELS = {
	color = rgb { 152 130 191 }
	color_ui = rgb { 198 169 248 }
}
ETH = {
	color = rgb { 152 130 191 }
	color_ui = rgb { 198 169 248 }
}
GUA = {
	color = rgb { 72 49 112 }
	color_ui = rgb { 72 49 112 }
}
HON = {
	color = rgb { 128 145 65 }
	color_ui = rgb { 128 145 65 }
}
IRQ = {
	color = rgb { 178 114 99 }
	color_ui = rgb { 231 148 129 }
}
JAP = {
	color = hsv { 0.05 0.3 1 }
	color_ui = hsv { 0.05 0.25 1.2 }
}
LIB = {
	color = rgb { 152 130 191 }
	color_ui = rgb { 198 169 248 }
}
MEX = {
	color = rgb { 104 152 83 }
	color_ui = rgb { 135 198 108 }
}
NEP = {
	color = rgb { 152 130 191 }
	color_ui = rgb { 198 169 248 }
}
NIC = {
	color = rgb { 146 179 191 }
	color_ui = rgb { 146 179 191 }
}
NZL = {
	color = rgb { 152 130 191 }
	color_ui = rgb { 198 169 248 }
}
PAN = {
	color = rgb { 152 130 191 }
	color_ui = rgb { 198 169 248 }
}
PER = {
	color = rgb { 35 159 64 }
	color_ui = rgb { 92 147 126 }
}
PHI = {
	color = rgb { 152 130 191 }
	color_ui = rgb { 198 169 248 }
}
PRU = {
	color = rgb { 196 189 204 }
	color_ui = rgb { 255 246 255 }
}
SAF = {
	color = rgb { 152 130 191 }
	color_ui = rgb { 198 169 248 }
}
SAU = {
	color = rgb { 0 127 14 }
	color_ui = rgb { 0 127 14 }
}
SIA = {
	color = rgb { 171 190 152 }
	color_ui = rgb { 222 247 198 }
}
SIK = {
	color = rgb { 71 190 152 }
	color_ui = rgb { 222 247 198 }
}
TIB = {
	color = rgb { 80 115 45}
	color_ui = rgb { 222 247 198 }
}
URG = {
	color = rgb { 171 190 152 }
	color_ui = rgb { 222 247 198 }
}
VEN = {
	color = rgb { 171 190 152 }
	color_ui = rgb { 222 247 198 }
}
YUN = {
	color = rgb { 114 148 80 }
	color_ui = rgb { 222 247 198 }
}
USA = {
	color = rgb { 40 50 122 }
	color_ui = rgb { 40 50 122 }
}
MON = {
	#color = HSV { 0.1 0.15 0.4 }
	color = rgb { 108 140 42 }
	color_ui = rgb { 209 247 133 }
}
MEN = {
	color = rgb { 185  255  152 }
	color_ui = rgb { 165  230  132 }
}
TAN = {
	color = rgb { 152 130 191 }
	color_ui = rgb { 198 169 248 }
}
PAR = {
	color = rgb { 57 113 228 }
	color_ui = rgb { 74 147 255 }
}
CUB = {
	color = rgb { 140 65 166 }
	color_ui = rgb { 140 65 166 }
}
DOM = {
	color = rgb { 152 130 191 }
	color_ui = rgb { 198 169 248 }
}
HAI = {
	color = rgb { 174 113 113 }
	color_ui = rgb { 174 113 113 }
}
YEM = {
	color = rgb { 155 101 107 }
	color_ui = rgb { 202 131 139 }
}
OMA = {
	color = rgb { 155 101 107 }
	color_ui = rgb { 202 131 139 }
}
SLO = {
	color = rgb { 158  161  188 }
	color_ui = rgb { 255 255 255 }
}
RAJ = {
	color = rgb { 242  121  0 }
	color_ui = rgb { 242  121  0 }
}

CRO = {
	color = rgb { 77  103  174 }
	color_ui = rgb { 77  103  174 }
}

PRC = {
	color = rgb { 188  123  103 }
	color_ui = rgb { 188  123  103 }
}

GXC = {
	color = rgb { 71 113 97 }
	color_ui = rgb { 71 113 97 }
}

SHX = {
	color = rgb { 82  2  15 }
	color_ui = rgb { 101  30  41 }
}

XSM = {
	color = rgb { 105 90 132 }
	color_ui = rgb { 105 90 132 }
}

LBA = {
	color = rgb { 200 180 90 }
	color_ui = rgb { 200 180 90 }
}

EGY = {
	color = rgb { 230 230 70 }
	color_ui = rgb { 230 230 70 }
}

PAL = {
	color = rgb { 170  125  80 }
	color_ui = rgb { 170  125  80 }
}

LEB = {
	color = rgb { 130  150  60 }
	color_ui = rgb { 130  150  60 }
}

JOR = {
	color = rgb { 111  55  78 }
	color_ui = rgb { 111  55  78 }
}

SYR = {
	color = rgb { 100  100  150 }
	color_ui = rgb { 100  100  150 }
}

SER = {
	color = rgb { 160 110 110 }
	color_ui = rgb { 160 110 110 }
}

ICE = {
	color = rgb { 62  129  155 }
	color_ui = rgb { 62  129  155 }
}

UKR = {
	color = rgb { 0  80  230 }
	color_ui = rgb { 0  80  230 }
}

GEO = {
	color = rgb { 255 236 236 }
	color_ui = rgb { 240 0 55 }
}

AZR = {
	color = rgb { 0 185 228 }
	color_ui = rgb { 95 5 135 }
}

ARM = {
	color = rgb { 255 100 0 }
	color_ui = rgb { 240 90 40 }
}

LAO = {
	color = rgb { 176  102  136 }
	color_ui = rgb { 176  102  136 }
}

INS = {
	color = rgb { 128  158  118 }
	color_ui = rgb { 128  158  118 }
}

VIN = {
	color = rgb { 230  223  50 }
	color_ui = rgb { 230  223  50 }
}

CAM = {
	color = rgb { 100  71  150 }
	color_ui = rgb { 100  71  150 }
}

MAL = {
	color = rgb { 213  169  121 }
	color_ui = rgb { 213  169  121 }
}

MNT = {
	color = rgb { 77  90  105 }
	color_ui = rgb { 77  90  105 }
}

BLR = {
	color = rgb { 255 100 50 }
	color_ui = rgb { 240 90 40 }
}
MAN = {
	color = rgb { 255 120 71 }
	color_ui = rgb { 255 120 71 }
}
ANG = {
	color = rgb { 37 140 61 }
	color_ui = rgb { 37 140 61 }
}

COG = {
	color = rgb { 152 159 209 }
	color_ui = rgb { 152 159 209 }
}
MZB = {
	color = rgb { 112 62 90 }
	color_ui = rgb { 112 62 90 }
}
KEN = {
	color = rgb { 145 103 14 }
	color_ui = rgb { 145 103 14 }
}
ZIM = {
	color = rgb { 7 7 239 }
	color_ui = rgb { 7 7 239 }
}
BOT = {
	color = rgb { 11 132 112 }
	color_ui = rgb { 11 132 112 }
}
PAK = {
	color = rgb { 36  235  26 }
	color_ui = rgb { 36  235  26 }
}
WGR = {
    color = rgb { 195 236 230 } # The colors are set to be more are less what we are used to
    color_ui = rgb { 225 255 255 }
}
DDR = {
    color = rgb { 120 150 80 }
    color_ui = rgb { 150 180 120 }
}
KOR = {
	color = rgb { 255  240  195 }
	color_ui = rgb { 255 248 229 }
}
ISR = {
	color = rgb { 255  240  195 }
	color_ui = rgb { 255 248 229 }
}
D01 = {
	color = rgb { 200 100 31 }
	color_ui = rgb { 245 123 38 }
}
D02 = {
	color = rgb { 80 150 179 }
	color_ui = rgb { 89 219 220 }
}
D03 = {
	color = rgb { 92 92 197 }
	color_ui = rgb { 116 116 247 }
}
D04 = {
	color = rgb { 154 69 116 }
	color_ui = rgb { 200 90 151 }
}
D05 = {
	color = rgb { 194 140 86 }
	color_ui = rgb { 252 182 112 }
}
D06 = {
	color = rgb { 195 149 155 }
	color_ui = rgb { 254 194 202 }
}
D07 = {
	color = rgb { 210 106 47 }
	color_ui = rgb { 255 138 61 }
}
D08 = {
	color = rgb { 105 147 86 }
	color_ui = rgb { 149 210 123 }
}
D09 = {
	color = rgb { 175 162 108 }
	color_ui = rgb { 221 203 135 }
}
D10 = {
	color = rgb { 124 140 162 }
	color_ui = rgb { 161 182 211 }
}
D11 = {
	color = rgb { 187 79 86 }
	color_ui = rgb { 243 103 112 }
}
D12 = {
	color = rgb { 69 157 208 }
	color_ui = rgb { 90 204 255 }
}
D13 = {
	color = rgb { 142 79 200 }
	color_ui = rgb { 176 98 249 }
}
D14 = {
	color = rgb { 122 179 97 }
	color_ui = rgb { 159 233 126 }
}
D15 = {
	color = rgb { 194 140 86 }
	color_ui = rgb { 252 182 112 }
}
GAG = {
	color = rgb { 29 29 196 }
	color_ui = rgb { 29 29 296 }
}
PMR = {
	color = rgb { 49 79 45 }
	color_ui = rgb { 49 79 45 }
}
MOL = {
	color = rgb { 255 182 10 }
	color_ui = rgb { 255 182 10 }
}
BOS = {
	color = rgb {102 255 102}
	color_ui = rgb { 102 255 102 }
}
RES = {
	color = rgb { 4 148 245 }
	color_ui = rgb { 4 148 245 }
}
CRB = {
	color = rgb { 245 105 0 }
	color_ui = rgb { 245 105 0 }
}
SLV = {
	color = rgb { 54  167  156  }
	color_ui = rgb { 54  167  156  }
}
UZB = {
	color = rgb { 128  0  0 }
	color_ui = rgb { 128  0  0 }
}
KGZ = {
	color = rgb { 055  183  106 }
	color_ui = rgb { 055  183  106 }
}
TJK = {
	color = rgb { 234  239  210 }
	color_ui = rgb { 234  239  210 }
}
TKM = {
	color = rgb  { 249  126  98 }
	color_ui = rgb  { 249  126  98 }
}
NGK = {
	color = rgb { 230 140 20 }
	color_ui = rgb { 220 130 10 }
}
SIN = {
	color = rgb { 215 198 134 }
	color_ui = rgb  { 215 198 134 }
}
SOS = {
	color = rgb { 225  230 143 }
	color_ui = rgb  { 255  90  0 }
}
PRK = {
	color = rgb  { 255  0  0 }
	color_ui = rgb   { 255  0  0 }
}
MKD = {
	color = rgb  { 173 255 47 }
	color_ui = rgb   { 173 255 47 }
}
GUB = {
	color = rgb { 103  102  101 }
	color_ui = rgb   { 103  102  101 }
}
GAM = {
	color = rgb { 57  108  108 }
	color_ui = rgb   { 57  108  108 }
}
SAR = {
	color = rgb { 40  152  185 }
	color_ui = rgb   { 40  152  185 }
}
GAN = {
	color = rgb { 169  048  051 }
	color_ui = rgb   { 169  048  051 }
}
NGR = {
	color = rgb { 000  135  081 }
	color_ui = rgb   { 000  135  081 }
}
RWA = {
	color = rgb { 219  83  106 }
	color_ui = rgb   { 219  83  106 }
}
BRN = {
	color = rgb { 208 137 16 }
	color_ui = rgb  { 208 137 16 }
}
EQG = {
	color = rgb { 51  75  144 }
	color_ui = rgb  { 51  75  144 }
}
GAB = {
	color = rgb { 58  117  196 }
	color_ui = rgb  { 58  117  196 }
}
DJI = {
	color = rgb { 102  148  107 }
	color_ui = rgb  { 102  148  107 }
}
SOM = {
	color = rgb { 011  133  191 }
	color_ui = rgb { 011  133  191 }
}
UGA = {
	color = rgb  { 243  209  016 }
	color_ui = rgb  { 243  209  016 }
}
NAM = {
	color = rgb { 112  095  111 }
	color_ui = rgb { 112  095  111 }
}
SWZ = {
	color = rgb  { 140  034  015 }
	color_ui = rgb  { 140  034  015 }
}
LES = {
	color = rgb { 15  25  15 }
	color_ui = rgb { 15  25  15 }
}
TNZ = {
	color = rgb { 30 181 58 }
	color_ui = rgb { 30 181 58 }
}
MLW = {
	color = rgb  { 165 157 168 }
	color_ui = rgb  { 165 157 168 }
}
ABK = {
	color = rgb { 200 100 31 }
	color_ui = rgb { 245 123 38 }
}
LKA = {
	color = rgb { 67  32  102 }
	color_ui = rgb { 67  32  102 }
}
RSK = {
	color = rgb { 4 148 245 }
	color_ui = rgb { 4 148 245 }
}
KUW = {
	color = rgb { 100  100  150 }
	color_ui = rgb { 100  100  150 }
}
BRU = {
	color = rgb { 200 100 31 }
	color_ui = rgb { 245 123 38 }
}
FIJ = {
    color = rgb { 4 148 245 }
    color_ui = rgb { 4 148 245 }
}
TIM = {
    color = rgb { 200 100 31 }
    color_ui = rgb { 245 123 38 }
}
MIV = {
    color = rgb { 112  095  111 }
    color_ui = rgb { 112  095  111 }
}
SEY = {
    color = rgb { 15  25  15 }
    color_ui = rgb { 15  25  15 }
}
BLI = {
    color = rgb { 102  148  107 }
    color_ui = rgb  { 102  148  107 }
}
QAT = {
    color = rgb { 212 31 31 }
    color_ui = rgb { 222 247 198 }
}
LIE = {
    color = rgb { 215 198 134 }
    color_ui = rgb  { 215 198 134 }
}
QAT = {
    color = rgb { 225  230 143 }
    color_ui = rgb  { 255  90  0 }
}
BHN = {
    color = rgb { 69 157 208 }
    color_ui = rgb { 90 204 255 }
}
SAN = {
    color = rgb { 230  223  50 }
    color_ui = rgb { 230  223  50 }
}
AND = {
    color = rgb  { 255  0  0 }
    color_ui = rgb   { 255  0  0 }
}
MNC = {
    color = rgb { 152 130 191 }
    color_ui = rgb { 198 169 248 }
}
VAT = {
    color = rgb { 195 149 155 }
    color_ui = rgb { 254 194 202 }
}
NAK = {
	color = rgb { 224 5 5 }
	color_ui = rgb { 193 81 81 }
}
GUG = {
	color = rgb { 100  100  150 }
	color_ui = rgb { 100  100  150 }
}
NCY = {
	color = rgb { 234 34 34 }
	color_ui = rgb { 234 34 34 }
}
CYP = {
    color = rgb { 255  255  255 }
    color_ui = rgb { 255  255  255 }
}
MTA = {
    color = rgb { 255 75 10 }
    color_ui = rgb { 255 75 10 }
}
BAN = {
    color = rgb { 0  97  28 }
    color_ui = rgb { 0  97  28 }
}
CHE = {
    color = rgb { 58  161  94 }
    color_ui = rgb { 58  161  94 }
}
KAB = {
    color = rgb { 3  255  82 }
    color_ui = rgb { 3  255  82 }
}
KCH = {
    color = rgb { 2  2  123 }
    color_ui = rgb { 2  2  123 }
}
NOS = {
	color = rgb { 225  230 143 }
	color_ui = rgb  { 225  230 143 }
}
ADY = {
	color = rgb { 18  102 89 }
	color_ui = rgb  { 18  102 89 }
}
ING = {
	color = rgb { 235  164 23 }
	color_ui = rgb  { 235  164 23 }
}
BAY = {
	color = rgb { 233  231  246 }
	color_ui = rgb { 233  231  246 }
}
MEK = {
	color = rgb { 186 245 86 }
	color_ui = rgb { 186 245 86 }
}
PRE = {
	color = rgb { 160  160  160 }
	color_ui = rgb { 160  160  160 }
}
SAX = {
	color = rgb { 102 102 0 }
	color_ui = rgb { 102 102 0 }
}
HAN = {
	color = rgb { 255 0 0 }
	color_ui = rgb { 255 0 0 }
}
WUR = {
	color = rgb { 255 204 153 }
	color_ui = rgb { 255 204 153 }
}
SHL = {
	color = rgb { 51 51 255 }
	color_ui = rgb { 51 51 255 }
}