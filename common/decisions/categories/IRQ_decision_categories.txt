################
#### Iraq ####
################

IRQ_war = {

	allowed = {
		original_tag = IRQ
	}
	
	visible = {
		NOT = { has_country_flag = no_conflict }
		date > 1992.01.01
	}
}