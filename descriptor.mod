version="0.1"
replace_path="history/states"
replace_path="history/countries"
replace_path="map/strategicregions"
replace_path="map/supplyareas"
replace_path="map/"
replace_path="common/countries"
replace_path="common/national_focus"
tags={
	"Historical"
	"Alternative History"
}
name="The New Era"
supported_version="1.10.3"
