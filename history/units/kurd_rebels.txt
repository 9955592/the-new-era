﻿division_template = {
	name = "Kurd Rebels"
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

		motorized = { x = 2 y = 0 }
	}
	support = 	{
		military_police = { x = 0 y = 0 }
	}
			priority = 1
}
units = {
###VV###
	division= {
		name = "1-st Rebel Division"
		location = 12773  
		division_template = "Kurd Rebels"
		force_equipment_variants = { infantry_equipment_1 = { owner = "IRQ" } }
		start_experience_factor = 0.2
		start_equipment_factor = 0.8
	}
	division= {
		name = "2-nd Rebel Division"
		location = 9816  
		division_template = "Kurd Rebels"
		force_equipment_variants = { infantry_equipment_1 = { owner = "IRQ" } }
		start_experience_factor = 0.2
		start_equipment_factor = 0.8
	}
}