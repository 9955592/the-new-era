### MOLainian OOB ###

division_template = {
	name = "VV"

	# Note: Represents both regular infantry and militia units
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

		motorized = { x = 2 y = 0 }
	}
	support = 	{
		military_police = { x = 0 y = 0 }
	}
			priority = 0
}
division_template = {
	name = "SpecNaz"

	# Note: Represents both regular infantry and militia units
	regiments = {
		mechanized = { x = 0 y = 0 }
		mechanized = { x = 0 y = 1 }

		motorized = { x = 1 y = 0 }
	}
	support = 	{
		recon = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
		military_police = { x = 0 y = 2 }
	}

}
division_template = {
	name = "Vozdusno-desantnaya diviziya"

	# Note: Represents both regular infantry and militia units
	regiments = {
		paratrooper = { x = 0 y = 0 }
		paratrooper = { x = 0 y = 1 }
		paratrooper = { x = 0 y = 2 }

		paratrooper = { x = 1 y = 0 }
		paratrooper = { x = 1 y = 1 }
		paratrooper = { x = 1 y = 2 }

		paratrooper = { x = 2 y = 0 }
		paratrooper = { x = 2 y = 1 }
		paratrooper = { x = 2 y = 2 }
	}
	support = 	{
		recon = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
		engineer = { x = 0 y = 2 }
		military_police = { x = 0 y = 3 }
	}

}
division_template = {
	name = "Okrema desantno-shturmova brygada"

	# Note: Represents both regular infantry and militia units
	regiments = {
		paratrooper = { x = 0 y = 0 }
		paratrooper = { x = 0 y = 1 }
		paratrooper = { x = 0 y = 2 }
	}
	support = 	{
	recon = { x = 0 y = 0 }
	engineer = { x = 0 y = 1 }
	artillery = { x = 0 y = 2 }
	anti_air = { x = 0 y = 3 }
	}
			priority = 2
}
division_template = {
	name = "Artyleriyska brygada"

	# Note: Represents both regular infantry and militia units
	regiments = {
		artillery_brigade = { x = 0 y = 0 }
		artillery_brigade = { x = 0 y = 1 }
		artillery_brigade = { x = 0 y = 2 }
		artillery_brigade = { x = 0 y = 3 }

		rocket_artillery_brigade = { x = 1 y = 0 }
		rocket_artillery_brigade = { x = 1 y = 1 }

		anti_air_brigade = { x = 2 y = 0 }
		anti_air_brigade = { x = 2 y = 1 }

		infantry = { x = 3 y = 0 }
		infantry = { x = 3 y = 1 }
	}
	support = 	{
		recon = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
		anti_air = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Zenitnaya raketnaya brygada"

	# Note: Represents both regular infantry and militia units
	regiments = {
		anti_air_brigade = { x = 0 y = 0 }
		anti_air_brigade = { x = 0 y = 1 }
		anti_air_brigade = { x = 0 y = 2 }
		anti_air_brigade = { x = 0 y = 3 }

		modern_sp_anti_air_brigade = { x = 1 y = 0 }
		modern_sp_anti_air_brigade = { x = 1 y = 1 }

		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
	}
	support = 	{
		recon = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Tankоva dyvisiya"
	division_names_group = BRENGL_INF_01
	regiments = {
			medium_armor = { x = 0 y = 0 }
			medium_armor = { x = 0 y = 1 }
			medium_armor = { x = 0 y = 2 }
			medium_armor = { x = 0 y = 3 }

			medium_armor = { x = 1 y = 0 }
			medium_armor = { x = 1 y = 1 }
			medium_armor = { x = 1 y = 2 }

			mechanized = { x = 2 y = 0 }
			mechanized = { x = 2 y = 1 }
			mechanized = { x = 2 y = 2 }
			mechanized = { x = 2 y = 3 }

			motorized = { x = 3 y = 0 }
			motorized = { x = 3 y = 1 }
	}
	support = 	{
	recon = { x = 0 y = 0 }
	engineer = { x = 0 y = 1 }
	artillery = { x = 0 y = 2 }
	anti_air = { x = 0 y = 3 }
	}

}
division_template = {
	name = "Mechanizovana dyviziya"
	division_names_group = BRENGL_INF_01
	regiments = {
			medium_armor = { x = 0 y = 0 }
			medium_armor = { x = 0 y = 1 }
			medium_armor = { x = 0 y = 2 }
			medium_armor = { x = 0 y = 3 }

			mechanized = { x = 1 y = 0 }
			mechanized = { x = 1 y = 1 }
			mechanized = { x = 1 y = 2 }
			mechanized = { x = 1 y = 3 }
			mechanized = { x = 1 y = 3 }

			motorized = { x = 2 y = 0 }
			motorized = { x = 2 y = 1 }

			modern_sp_artillery_brigade = { x = 3 y = 0 }
			modern_sp_artillery_brigade = { x = 3 y = 1 }

			modern_sp_anti_air_brigade = { x = 4 y = 0 }
	}
	support = 	{
	recon = { x = 0 y = 0 }
	engineer = { x = 0 y = 1 }
	artillery = { x = 0 y = 2 }
	anti_air = { x = 0 y = 3 }
	anti_tank = { x = 0 y = 4 }
	}

}
division_template = {
	name = "Motostrilecka Dyviziya"
	division_names_group = BRENGL_INF_01

	regiments = {
		motorized = { x = 0 y = 0 }
	  motorized = { x = 0 y = 1 }
		motorized = { x = 0 y = 2 }

		motorized = { x = 1 y = 0 }
		motorized = { x = 1 y = 1 }
		motorized = { x = 1 y = 2 }

		motorized = { x = 2 y = 0 }
	  motorized = { x = 2 y = 1 }
		motorized = { x = 2 y = 2 }

		motorized = { x = 3 y = 0 }
		motorized = { x = 3 y = 1 }
		motorized = { x = 3 y = 2 }
	}
	support = {
    recon = { x = 0 y = 0 }
	 	engineer = { x = 0 y = 1 }
		engineer = { x = 0 y = 2 }
    anti_air = { x = 0 y = 3 }
	}

}



units = {
###Motostrelkovaya Brigada###
	division= {
		name = "86ya gv. motostrelkovaya diviziya"
		location = 6600  #Byeltsy
		division_template = "Motostrilecka Dyviziya"
		force_equipment_variants = { infantry_equipment_1 = { owner = "MOL" } }
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}
###VV###
	division= {
		name = "70ya otdelnaya brigada VV"
		location = 11686  #Kishenyov
		division_template = "VV"
		force_equipment_variants = { infantry_equipment_1 = { owner = "MOL" } }
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}
	division= {
		name = "79y pogranichnyy otryad"
		location = 11686  #Kishenyov
		division_template = "VV"
		force_equipment_variants = { infantry_equipment_1 = { owner = "MOL" } }
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}
###Zenitno-raketnaya###
	division= {
		name = "275ya gv. zenitno-raketnaya brigada"
		location = 11686  #Kishenyov
		division_template = "Zenitnaya raketnaya brygada"
		force_equipment_variants = { infantry_equipment_1 = { owner = "MOL" } }
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}
}


### No air forces (small handful of various aircraft in 1935) ###

#########################
## STARTING PRODUCTION ##
#########################

instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_1
			creator = "MOL"
		}
		requested_factories = 1
		progress = 0.80
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = support_equipment_1
			creator = "MOL"
		}
		requested_factories = 1
		progress = 0.80
		efficiency = 100
	}
}

#################################
