﻿### Ukrainian OOB ###

division_template = {
	name = "Garnizonna Brygada"

	# Note: Represents both regular infantry and militia units
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

		motorized = { x = 2 y = 0 }
	}
	support = 	{
		military_police = { x = 0 y = 0 }
	}
			priority = 0
}
division_template = {
	name = "SpecNaz"

	# Note: Represents both regular infantry and militia units
	regiments = {
		mechanized = { x = 0 y = 0 }
		mechanized = { x = 0 y = 1 }

		motorized = { x = 1 y = 0 }
	}
	support = 	{
		recon = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
		military_police = { x = 0 y = 2 }
	}
			priority = 0
}
division_template = {
	name = "Okrema desantno-shturmova brygada"

	# Note: Represents both regular infantry and militia units
	regiments = {
		paratrooper = { x = 0 y = 0 }
		paratrooper = { x = 0 y = 1 }
		paratrooper = { x = 0 y = 2 }
	}
	support = 	{
	recon = { x = 0 y = 0 }
	engineer = { x = 0 y = 1 }
	artillery = { x = 0 y = 2 }
	anti_air = { x = 0 y = 3 }
	}
			priority = 2
}
division_template = {
	name = "Artyleriyska brygada"

	# Note: Represents both regular infantry and militia units
	regiments = {
		artillery_brigade = { x = 0 y = 0 }
		artillery_brigade = { x = 0 y = 1 }
		artillery_brigade = { x = 0 y = 2 }
		artillery_brigade = { x = 0 y = 3 }

		rocket_artillery_brigade = { x = 1 y = 0 }
		rocket_artillery_brigade = { x = 1 y = 1 }

		anti_air_brigade = { x = 2 y = 0 }
		anti_air_brigade = { x = 2 y = 1 }

		infantry = { x = 3 y = 0 }
		infantry = { x = 3 y = 1 }
	}
	support = 	{
		recon = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
		anti_air = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Zenitnaya raketnaya brygada"

	# Note: Represents both regular infantry and militia units
	regiments = {
		anti_air_brigade = { x = 0 y = 0 }
		anti_air_brigade = { x = 0 y = 1 }
		anti_air_brigade = { x = 0 y = 2 }
		anti_air_brigade = { x = 0 y = 3 }

		modern_sp_anti_air_brigade = { x = 1 y = 0 }
		modern_sp_anti_air_brigade = { x = 1 y = 1 }

		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
	}
	support = 	{
		recon = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Tankоva dyvisiya"
	division_names_group = BRENGL_INF_01
	regiments = {
			medium_armor = { x = 0 y = 0 }
			medium_armor = { x = 0 y = 1 }
			medium_armor = { x = 0 y = 2 }
			medium_armor = { x = 0 y = 3 }

			medium_armor = { x = 1 y = 0 }
			medium_armor = { x = 1 y = 1 }
			medium_armor = { x = 1 y = 2 }

			mechanized = { x = 2 y = 0 }
			mechanized = { x = 2 y = 1 }
			mechanized = { x = 2 y = 2 }
			mechanized = { x = 2 y = 3 }

			motorized = { x = 3 y = 0 }
			motorized = { x = 3 y = 1 }
	}
	support = 	{
	recon = { x = 0 y = 0 }
	engineer = { x = 0 y = 1 }
	artillery = { x = 0 y = 2 }
	anti_air = { x = 0 y = 3 }
	}

}
division_template = {
	name = "Mechanizovana dyviziya"
	division_names_group = BRENGL_INF_01
	regiments = {
			medium_armor = { x = 0 y = 0 }
			medium_armor = { x = 0 y = 1 }
			medium_armor = { x = 0 y = 2 }
			medium_armor = { x = 0 y = 3 }

			mechanized = { x = 1 y = 0 }
			mechanized = { x = 1 y = 1 }
			mechanized = { x = 1 y = 2 }
			mechanized = { x = 1 y = 3 }
			mechanized = { x = 1 y = 3 }

			motorized = { x = 2 y = 0 }
			motorized = { x = 2 y = 1 }

			modern_sp_artillery_brigade = { x = 3 y = 0 }
			modern_sp_artillery_brigade = { x = 3 y = 1 }

			modern_sp_anti_air_brigade = { x = 4 y = 0 }
	}
	support = 	{
	recon = { x = 0 y = 0 }
	engineer = { x = 0 y = 1 }
	artillery = { x = 0 y = 2 }
	anti_air = { x = 0 y = 3 }
	anti_tank = { x = 0 y = 4 }
	}

}
division_template = {
	name = "Motostrilecka Dyviziya"
	division_names_group = BRENGL_INF_01

	regiments = {
		motorized = { x = 0 y = 0 }
	  motorized = { x = 0 y = 1 }
		motorized = { x = 0 y = 2 }

		motorized = { x = 1 y = 0 }
		motorized = { x = 1 y = 1 }
		motorized = { x = 1 y = 2 }

		motorized = { x = 2 y = 0 }
	  motorized = { x = 2 y = 1 }
		motorized = { x = 2 y = 2 }

		motorized = { x = 3 y = 0 }
		motorized = { x = 3 y = 1 }
		motorized = { x = 3 y = 2 }
	}
	support = {
    recon = { x = 0 y = 0 }
	 	engineer = { x = 0 y = 1 }
		engineer = { x = 0 y = 2 }
    anti_air = { x = 0 y = 3 }
	}

}



units = {
division= {
	name = "17ya tankovaja diviziya"
	location = 11437
	division_template = "Tankоva dyvisiya"
	start_experience_factor = 0.2
}
division= {
	name = "30ya tankovaja diviziya"
	location = 6593
	division_template = "Tankоva dyvisiya"
	start_experience_factor = 0.2
}
division= {
	name = "119y uchebnyy centr"
	location = 6593
	division_template = "Tankоva dyvisiya"
	start_experience_factor = 0.2
}
division= {
	name = "5193ya BHVT"
	location = 9451
	division_template = "Tankоva dyvisiya"
	start_experience_factor = 0.2
}
division= {
	name = "6065ya BHVT"
	location = 6593
	division_template = "Tankоva dyvisiya"
	start_experience_factor = 0.2
}
division= {
	name = "48ya motostrelk. div. SpN"
	location = 418
	division_template = "Motostrilecka Dyviziya"
	start_experience_factor = 0.2
}
division= {
	name = "1ya motoriz. diviziya VV"
	location = 525
	division_template = "Motostrilecka Dyviziya"
	start_experience_factor = 0.2
}
division= {
	name = "2ya motoriz. diviziya VV"
	location = 418
	division_template = "Motostrilecka Dyviziya"
	start_experience_factor = 0.2
}
division= {
	name = "3ya motoriz. diviziya VV"
	location = 11670
	division_template = "Motostrilecka Dyviziya"
	start_experience_factor = 0.2
}
division= {
	name = "4ya motoriz. diviziya VV"
	location = 6474
	division_template = "Motostrilecka Dyviziya"
	start_experience_factor = 0.2
}
division= {
	name = "5ya motoriz. diviziya VV"
	location = 11479
	division_template = "Motostrilecka Dyviziya"
	start_experience_factor = 0.2
}
division= {
	name = "6ya motoriz. diviziya VV"
	location = 11649
	division_template = "Motostrilecka Dyviziya"
	start_experience_factor = 0.2
}
division= {
	name = "7ya otd. konvoynaya brigada VV"
	location = 525
	division_template = "Garnizonna Brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "290y otd. polk VV	"
	location = 525
	division_template = "Garnizonna Brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "70ya otd. uchebnaya brigada VV"
	location = 11479
	division_template = "Garnizonna Brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "7y pogranichnyy otryad"
	location = 11479
	division_template = "Garnizonna Brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "26y pogranichnyy otryad"
	location = 11670
	division_template = "Garnizonna Brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "27y pogranichnyy otryad"
	location = 9563
	division_template = "Garnizonna Brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "31y pogranichnyy otryad"
	location = 577
	division_template = "Garnizonna Brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "79y pogranichnyy otryad"
	location = 11649
	division_template = "Garnizonna Brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "16ya otd. konvoynaya brigada VV"
	location = 11479
	division_template = "Garnizonna Brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "17ya otd. konvoynaya brigada VV"
	location = 11670
	division_template = "Garnizonna Brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "18ya otd. konvoynaya brigada VV"
	location = 6474
	division_template = "Garnizonna Brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "20ya otd. konvoynaya brigada VV"
	location = 418
	division_template = "Garnizonna Brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "93ya otd. konvoynaya brigada VV"
	location = 11437
	division_template = "Garnizonna Brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "23ya otd. desantno-shturmovaya brigada"
	location = 511
	division_template = "Okrema desantno-shturmova brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "40ya otd. desantno-shturmovaya brigada"
	location = 11683
	division_template = "Okrema desantno-shturmova brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "95y  uchebnyy centr VDV"
	location = 3520
	division_template = "Okrema desantno-shturmova brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "217y parashutno-desantnyy polk"
	location = 11437
	division_template = "Okrema desantno-shturmova brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "224y uchebnyy centr VDV"
	location = 11479
	division_template = "Okrema desantno-shturmova brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "299y parashutno-desantnyy polk"
	location = 11670
	division_template = "Okrema desantno-shturmova brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
###Mechanized###
division= {
	name = "17ya motostrelkovaya diviziya"
	location = 6429  #Khmelnitskiy
	division_template = "Mechanizovana dyviziya"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "24ya motostrelkovaya diviziya"
	location = 11479 #Lvov
	division_template = "Mechanizovana dyviziya"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "25ya motostrelkovaya diviziya"
	location = 511 #Poltava
	division_template = "Mechanizovana dyviziya"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "28ya motostrelkovaya diviziya"
	location = 11670 #Odessa
	division_template = "Mechanizovana dyviziya"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "36ya motostrelkovaya diviziya"
	location = 6474 #Donetsk
	division_template = "Mechanizovana dyviziya"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "51ya motostrelkovaya diviziya"
	location = 513 #Lutsk
	division_template = "Mechanizovana dyviziya"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "70ya motostrelkovaya diviziya"
	location = 11411 #Ivano-Frankovsk
	division_template = "Mechanizovana dyviziya"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "72ya motostrelkovaya diviziya"
	location = 504 #Bila Cerkva
	division_template = "Mechanizovana dyviziya"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "93ya motostrelkovaya diviziya"
	location = 11437  #Dnipropetrovsk
	division_template = "Mechanizovana dyviziya"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "97ya motostrelkovaya diviziya"
	location = 6429 #Khmelnitskiy
	division_template = "Mechanizovana dyviziya"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "126ya motostrelkovaya diviziya"
	location = 11649 #Simferopol
	division_template = "Mechanizovana dyviziya"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "128ya motostrelkovaya diviziy"
	location = 11691 #Uzhgorod
	division_template = "Mechanizovana dyviziya"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "161ya motostrelkovaya diviziya"
	location = 6429 #Khmelnitskiy
	division_template = "Mechanizovana dyviziya"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "180ya motostrelkovaya diviziya"
	location = 11670 #Odessa
	division_template = "Mechanizovana dyviziya"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "254ya motostrelkovaya diviziya"
	location = 6474 #Donetsk
	division_template = "Mechanizovana dyviziya"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "92y uchebnyy centr"
	location = 11683  #Mykolayiv
	division_template = "Mechanizovana dyviziya"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "110y uchebnyy centr"
	location = 577  #Chernivtsi
	division_template = "Mechanizovana dyviziya"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "169y uchebnyy centr"
	location = 6458 #Chernigov
	division_template = "Mechanizovana dyviziya"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
###Artillery###
division= {
	name = "26ya artileriyskaya diviziya"
	location = 3483 #Ternopol
	division_template = "Artyleriyska brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "55ya artileriyskaya diviziya"
	location = 11405 #Zaporozhye
	division_template = "Artyleriyska brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "182ya otd. artileriyskaya brigada"
	location = 6458 #Lugansk
	division_template = "Artyleriyska brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "184ya otd. artileriyskaya brigada"
	location = 9461 #Odessa
	division_template = "Artyleriyska brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "188ya otd. artileriyskaya brigada"
	location = 6593 #Zhytomyr
	division_template = "Artyleriyska brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "190ya otd. artileriyskaya brigada"
	location = 11670 #Odessa
	division_template = "Artyleriyska brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "192ya otd. artileriyskaya brigada"
	location = 525 #Kiev
	division_template = "Artyleriyska brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "222ya otd. artileriyskaya brigada"
	location = 11437 #Dnipropetrovsk
	division_template = "Artyleriyska brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "238ya otd. artileriyskaya brigada"
	location = 737 #Kherson
	division_template = "Artyleriyska brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "265ya otd. artileriyskaya brigada"
	location = 418 #Kharkov
	division_template = "Artyleriyska brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "301ya artileriyskaya brigada VMS"
	location = 11649 #Simferopol
	division_template = "Artyleriyska brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "337ya otd. artileriyskaya brigada"
	location = 11479 #Lviv
	division_template = "Artyleriyska brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
###Rocket Artillery###
division= {
	name = "96ya zenitnaya raketnaya brigada"
	location = 525 #Kiev
	division_template = "Zenitnaya raketnaya brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "100ya zenitnaya raketnaya brigada"
	location = 11405 #Zaporozhye
	division_template = "Zenitnaya raketnaya brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "148ya zenitnaya raketnaya brigada"
	location = 418 #Kharkov
	division_template = "Zenitnaya raketnaya brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "160ya zenitnaya raketnaya brigada"
	location = 11670 #Odessa
	division_template = "Zenitnaya raketnaya brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "174ya zenitnaya raketnaya brigada"
	location = 11649 #Krym
	division_template = "Zenitnaya raketnaya brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "206ya zenitnaya raketnaya brigada"
	location = 11649 #Krym
	division_template = "Zenitnaya raketnaya brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "208ya zenitnaya raketnaya brigada"
	location = 525 #Kiev
	division_template = "Zenitnaya raketnaya brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "212ya zenitnaya raketnaya brigada"
	location = 6420 #Mariupol
	division_template = "Zenitnaya raketnaya brygada"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
###SpecNaz###
division= {
	name = "8ya otd. brigada SpN"
	location = 6429 #Khmelnitskiy
	division_template = "SpecNaz"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "50y otd. uchebnyy otryad SpN"
	location = 3468 #Kirovograd
	division_template = "SpecNaz"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "10ya otd. brigada SpN"
	location = 3468 #Kirovograd
	division_template = "SpecNaz"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
division= {
	name = "1464y otd. morsk. razved. punkt SpN"
	location = 11683 #Nikolaev
	division_template = "SpecNaz"
	force_equipment_variants = { infantry_equipment_1 = { owner = "UKR" } }
	start_experience_factor = 0.2
	start_equipment_factor = 0.5
}
}


### No air forces (small handful of various aircraft in 1935) ###

#########################
## STARTING PRODUCTION ##
#########################

instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_1
			creator = "UKR"
		}
		requested_factories = 1
		progress = 0.80
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = support_equipment_1
			creator = "UKR"
		}
		requested_factories = 1
		progress = 0.80
		efficiency = 100
	}
}

#################################
