﻿capital = 455

# Starting tech
set_technology = {
	infantry_weapons = 1
	gwtank = 1
}

set_convoys = 5

set_politics = {
	ruling_party = authoritarism
	last_election = "1990.1.1"
	election_frequency = 48
	elections_allowed = yes
}
set_popularities = {
	left_tolitarism = 0
	pragmatic_socialism = 0
	social_democratic = 0
	centrism = 0
	liberalism = 0
	conservatism = 0
	paternalism = 0
	authoritarism = 100
	nationalism = 0
}