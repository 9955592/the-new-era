﻿capital = 163

oob = "RSK_1936"
set_research_slots = 2
set_convoys = 180
add_political_power = 200
set_stability = 0.8
set_war_support = 0.1
set_technology = {	
}

set_politics = {
	ruling_party = nationalism
	last_election = "1990.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	left_tolitarism = 0
	pragmatic_socialism = 0
	social_democratic = 0
	centrism = 0
	liberalism = 0
	conservatism = 0
	paternalism = 0
	authoritarism = 0
	nationalism = 100
}

create_country_leader = { 
	name = "Milan Babić" 
	desc = "" 
	picture = "gfx/leaders/balkans/Portrait_Krajina_Milan_Babic.dds" 
	expire = "1992.02.16" 
	ideology = conservatism
	traits = { 
		# 
	} 
}

