﻿capital = 229 #Baku

oob = "AZR_1936"

set_research_slots = 3

# Starting tech
set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_recon = 1
	tech_support = 1
	tech_engineers = 1
	tech_military_police = 1
	tech_mountaineers = 1
	motorised_infantry = 1
	paratroopers = 1
	gw_artillery = 1
	gwtank = 1
	basic_light_tank = 1  # PLACEHOLDER
	#basic_heavy_tank = 1  # PLACEHOLDER
	#basic_medium_tank = 1 # PLACEHOLDER
	early_fighter = 1
	fighter1 = 1
	early_bomber = 1
	strategic_bomber1 = 1
	naval_bomber1 = 1
	early_submarine = 1
	basic_submarine = 1
	early_destroyer = 1
	early_light_cruiser = 1
	early_heavy_cruiser = 1
	early_battleship = 1
	early_battlecruiser = 1
	transport = 1
	mass_assault = 1
	fleet_in_being = 1
}
set_politics = {
	ruling_party = liberalism
	last_election = "1990.1.1"
	election_frequency = 48
	elections_allowed = yes
}
set_popularities = {
	left_tolitarism = 0
	pragmatic_socialism = 0
	social_democratic = 25
	centrism = 25
	liberalism = 25
	conservatism = 25
	paternalism = 0
	authoritarism = 0
	nationalism = 0
}

add_ideas = {
	SOV_post_soviet_state
}

create_country_leader = {
	name = "Ayaz Mutallibov"
	desc = ""
	picture = "gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
	expire = "1953.3.1"
	ideology = conservatism
	traits = {
	}
}
