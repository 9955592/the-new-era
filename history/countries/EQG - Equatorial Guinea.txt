﻿capital = 297

oob = "EQG_1936"
set_research_slots = 2
set_convoys = 10
add_political_power = 200
set_stability = 0.8
set_war_support = 0.1
set_politics = {
	ruling_party = liberalism
	last_election = "1990.1.1"
	election_frequency = 48
	elections_allowed = yes
}
set_popularities = {
	left_tolitarism = 0
	pragmatic_socialism = 0
	social_democratic = 25
	centrism = 25
	liberalism = 25
	conservatism = 25
	paternalism = 0
	authoritarism = 0
	nationalism = 0
}

create_country_leader = {
	name = "Teodoro Obiang Nguema Mbasogo"
	desc = ""
	picture = ""
	ideology = centrism
	traits = {
		#
	}
}
