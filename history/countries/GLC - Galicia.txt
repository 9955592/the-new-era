﻿capital = 171

#oob = ""

# Starting tech
set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_recon = 1
	tech_support = 1		
	tech_engineers = 1
	tech_mountaineers = 1
	gw_artillery = 1
	interwar_antiair = 1
	early_fighter = 1
	early_bomber = 1
	naval_bomber1 = 1
}

set_convoys = 20

set_politics = {
	ruling_party = liberalism
	last_election = "1990.1.1"
	election_frequency = 48
	elections_allowed = yes
}
set_popularities = {
	left_tolitarism = 0
	pragmatic_socialism = 0
	social_democratic = 25
	centrism = 25
	liberalism = 25
	conservatism = 25
	paternalism = 0
	authoritarism = 0
	nationalism = 0
}

create_country_leader = {
	name = "Andreu Nin i Pérez"
	picture = "gfx//leaders//Europe//Portrait_Europe_Generic_1.dds"
	expire = "1965.1.1"
	ideology = marxism
}

create_country_leader = {
	name = "Fuco Gómez"
	picture = "gfx//leaders//Europe//portrait_europe_generic_5.dds"
	expire = "1965.1.1"
	ideology = liberalism
}

create_country_leader = {
	name = "Vicente Martínez Risco"
	picture = "gfx//leaders//Europe//portrait_europe_generic_land_13.dds"
	expire = "1965.1.1"
	ideology = fascism_ideology
}

create_country_leader = {
	name = "Santiago Casares Quiroga"
	picture = "gfx//leaders//Europe//portrait_europe_generic_6.dds"
	expire = "1965.1.1"
	ideology = centrism
}
