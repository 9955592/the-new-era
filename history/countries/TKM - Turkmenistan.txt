﻿capital = 584

oob = "TKM_1939"

set_research_slots = 2
set_convoys = 10
add_political_power = 200
set_stability = 0.5
set_war_support = 0.3

set_politics = {
	ruling_party = authoritarism
	last_election = "1990.1.1"
	election_frequency = 48
	elections_allowed = yes
}
set_popularities = {
	left_tolitarism = 0
	pragmatic_socialism = 0
	social_democratic = 0
	centrism = 0
	liberalism = 0
	conservatism = 0
	paternalism = 0
	authoritarism = 100
	nationalism = 0
}

add_ideas = {
	SOV_post_soviet_state
}
