﻿capital = 48

oob = "BUL_1936"

add_ideas = {
	limited_conscription
}

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_support = 1		
	tech_engineers = 1
	motorised_infantry = 1
	early_fighter = 1
	fuel_silos = 1
}

1939.1.1 = {

	add_political_power = 1198
	
	#generic focuses
	complete_national_focus = army_effort
	complete_national_focus = equipment_effort
	complete_national_focus = motorization_effort
	complete_national_focus = aviation_effort
	complete_national_focus = flexible_navy
	complete_national_focus = industrial_effort
	complete_national_focus = construction_effort
	complete_national_focus = production_effort
	
	oob = "BUL_1939"
	set_technology = {
		fighter1 = 1
		early_bomber = 1
		tactical_bomber1 = 1
		gwtank = 1
		basic_light_tank = 1
		gw_artillery = 1
		interwar_artillery = 1
		tech_recon = 1
		infantry_weapons2 = 1
		support_weapons = 1

		#doctrines
		#Air
		force_rotation = 1
		fighter_baiting = 1
				
		grand_battle_plan = 1
		trench_warfare = 1
		fleet_in_being = 1
		battlefleet_concentration = 1
		convoy_sailing = 1

		#electronics
		electronic_mechanical_engineering = 1
		radio = 1
		radio_detection = 1
		mechanical_computing = 1
		computing_machine = 1

		#industry
		basic_machine_tools = 1
		improved_machine_tools = 1
		advanced_machine_tools = 1
		fuel_refining = 1
		construction1 = 1
		construction2 = 1
		dispersed_industry = 1
		dispersed_industry2 = 1
	}
}

set_convoys = 40

set_politics = {
	ruling_party = conservatism
	last_election = "1990.1.1"
	election_frequency = 24
	elections_allowed = no
}

set_popularities = {
	social_democratic = 30
	liberalism = 17
	conservatism = 45
	left_tolitarism = 8
}


create_country_leader = {
	name = "Simeon II"
	desc = "POLITICS_BORIS_III_DESC"
	picture = "Portrait_Bulgaria_Boris_III.dds"
	expire = "1965.1.1"
	ideology = authoritarism_type
	traits = {
		#
	}
}

create_country_leader = {
					name = "Velko Valkanov"
					desc = "POLITICS_MOHAMMED_ZAHIR_SHAH_DESC"
					picture = "Velko_Valkanov.dds"
					expire = "1965.1.1"
					ideology = social_democratic_type
				traits = {
					
				}
			}

create_country_leader = {
					name = "Zhelyu Zhelev"
					desc = "POLITICS_MOHAMMED_ZAHIR_SHAH_DESC"
					picture = "Zhelyu_Zhelev.dds"
					expire = "1965.1.1"
					ideology = conservatism_type
				traits = {
					
				}
			}
			
create_country_leader = {
					name = "Krassimir Karakachanov"
					desc = "POLITICS_MOHAMMED_ZAHIR_SHAH_DESC"
					picture = "Krasimir_Karakachanov.dds"
					expire = "1965.1.1"
					ideology = nationalism_type
				traits = {
					
				}
			}

create_corps_commander = {
	name = "Vasil Tenev Boydev"
	portrait_path = "gfx/leaders/Europe/Portrait_Europe_Generic_land_1.dds"
	traits = { armor_officer }
	skill = 3
	attack_skill = 2
	defense_skill = 3
	planning_skill = 2
	logistics_skill = 3
}

create_field_marshal = {
	name = "Georgi Nikolov Popov"
	portrait_path = "gfx/leaders/Europe/Portrait_Europe_Generic_land_2.dds"
	traits = { defensive_doctrine }
	skill = 4
	attack_skill = 2
	defense_skill = 4
	planning_skill = 3
	logistics_skill = 4
}