﻿capital = 742

oob = "TJK_1936"

set_research_slots = 2
set_convoys = 10
add_political_power = 200
set_stability = 0.8
set_war_support = 0.1

# Starting tech
set_technology = {
	infantry_weapons = 1
}

set_convoys = 5

set_politics = {
	ruling_party = authoritarism
	last_election = "1990.1.1"
	election_frequency = 48
	elections_allowed = yes
}
set_popularities = {
	left_tolitarism = 0
	pragmatic_socialism = 0
	social_democratic = 0
	centrism = 0
	liberalism = 43
	conservatism = 0
	paternalism = 0
	authoritarism = 57
	nationalism = 0
}

add_ideas = {
CIS
Postsovet
}
