﻿capital = 804

oob = "CRB_1936"




set_technology = {
}

set_research_slots = 3
set_politics = {
	ruling_party = liberalism
	last_election = "1990.1.1"
	election_frequency = 48
	elections_allowed = yes
}
set_popularities = {
	left_tolitarism = 0
	pragmatic_socialism = 0
	social_democratic = 25
	centrism = 25
	liberalism = 25
	conservatism = 25
	paternalism = 0
	authoritarism = 0
	nationalism = 0
}

create_country_leader = {
	name = "Mate Boban"
	desc = ""
	picture = "gfx/leaders/leader_unknown.dds"
	ideology = conservatism
	traits = {
		#
	}
}
