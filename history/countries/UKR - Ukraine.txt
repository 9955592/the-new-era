﻿capital = 202 #Kiev

oob = "UKR_1936"

set_research_slots = 3
###Thechs###
set_technology = {
	#infantry
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	improved_infantry_weapons = 1
	improved_infantry_weapons_2 = 1
	infantry_at2 = 1
	infantry_at = 1
	advanced_infantry_weapons = 1
	advanced_infantry_weapons2 = 1
	paratroopers = 1
	paratroopers2 = 1
	paratroopers3 = 1
	marines = 1
	marines2 = 1
	marines3 = 1
	tech_mountaineers = 1
	tech_mountaineers2 = 1
	tech_mountaineers3 = 1
	night_vision = 1
	night_vision2 = 1
	support_weapons = 1
	support_weapons2 = 1
	support_weapons3 = 1
	support_weapons4 = 1
	motorised_infantry = 1
	motorized_rocket_unit = 1
	mechanised_infantry = 1
	mechanised_infantry2 = 1
	mechanised_infantry3 = 1

	#support
	tech_support = 1
	tech_engineers = 1
	tech_engineers2 = 1
	tech_engineers3 = 1
	tech_engineers4 = 1
	tech_recon = 1
	tech_recon2 = 1
	tech_recon3 = 1
	tech_recon4 = 1
	tech_military_police = 1
	tech_military_police2 = 1
	tech_military_police3 = 1
	tech_military_police4 = 1
	tech_maintenance_company = 1
	tech_maintenance_company2 = 1
	tech_maintenance_company3 = 1
	tech_maintenance_company4 = 1
	tech_field_hospital = 1
	tech_field_hospital2 = 1
	tech_field_hospital3 = 1
	tech_field_hospital4 = 1
	tech_logistics_company = 1
	tech_logistics_company2 = 1
	tech_logistics_company3 = 1
	tech_logistics_company4 = 1
	tech_signal_company = 1
	tech_signal_company2 = 1
	tech_signal_company3 = 1
	tech_signal_company4 = 1

	#armor
	gwtank = 1
	basic_light_tank = 1
	basic_light_td = 1
	basic_light_art = 1
	basic_light_spaa = 1
	improved_light_tank = 1
	improved_light_td = 1
	improved_light_art = 1
	improved_light_spaa = 1
	advanced_light_tank = 1
	advanced_light_td = 1
	advanced_light_art = 1
	advanced_light_spaa = 1
	basic_medium_tank = 1
	basic_medium_td = 1
	basic_medium_art = 1
	basic_medium_spaa = 1
	improved_medium_tank = 1
	improved_medium_td = 1
	improved_medium_art = 1
	improved_medium_spaa = 1
	advanced_medium_tank = 1
	advanced_medium_td = 1
	advanced_medium_art = 1
	advanced_medium_spaa = 1
	main_battle_tank = 1
	modern_td = 1
	modern_art = 1
	modern_spaa = 1
	basic_heavy_tank = 1
	basic_heavy_td = 1
	basic_heavy_art = 1
	basic_heavy_spaa = 1
	improved_heavy_tank = 1
	improved_heavy_td = 1
	improved_heavy_art = 1
	improved_heavy_spaa = 1
	advanced_heavy_tank = 1
	advanced_heavy_td = 1
	advanced_heavy_art = 1
	advanced_heavy_spaa = 1
	super_heavy_tank = 1
	super_heavy_td = 1
	super_heavy_art = 1
	super_heavy_spaa = 1

	#artillery
	gw_artillery = 1
	interwar_artillery = 1
	artillery1 = 1
	artillery2 = 1
	artillery3 = 1
	artillery4 = 1
	artillery5 = 1
	rocket_artillery = 1
	rocket_artillery2 = 1
	rocket_artillery3 = 1
	rocket_artillery4 = 1
	interwar_antiair = 1
	antiair1 = 1
	antiair2 = 1
	antiair3 = 1
	antiair4 = 1
	antiair5 = 1
	interwar_antitank = 1
	antitank1 = 1
	antitank2 = 1
	antitank3 = 1
	antitank4 = 1
	antitank5 = 1

	#support
	tech_support = 1
	tech_engineers = 1
	tech_engineers2 = 1
	tech_engineers3 = 1
	tech_engineers4 = 1
	tech_recon = 1
	tech_recon2 = 1
	tech_recon3 = 1
	tech_recon4 = 1
	tech_military_police = 1
	tech_military_police2 = 1
	tech_military_police3 = 1
	tech_military_police4 = 1
	tech_maintenance_company = 1
	tech_maintenance_company2 = 1
	tech_maintenance_company3 = 1
	tech_maintenance_company4 = 1
	tech_field_hospital = 1
	tech_field_hospital2 = 1
	tech_field_hospital3 = 1
	tech_field_hospital4 = 1
	tech_logistics_company = 1
	tech_logistics_company2 = 1
	tech_logistics_company3 = 1
	tech_logistics_company4 = 1
	tech_signal_company = 1
	tech_signal_company2 = 1
	tech_signal_company3 = 1
	tech_signal_company4 = 1
}

set_politics = {
	ruling_party = neutrality
	last_election = "1936.9.1"
	election_frequency = 48
	elections_allowed = yes
}
set_popularities = {
	democratic = 10
	fascism = 5
	communism = 20
	neutrality = 65
}

add_ideas = {
	SOV_post_soviet_state
}

create_country_leader = {
	name = "Leonid Kravchuk"
	desc = ""
	picture = "Kravchuk.dds"
  expire = "1999.31.12"
	ideology = despotism
	traits = {
	}
}
create_country_leader = {
	name = "Petro Symonenko"
	desc = ""
	picture = "Symonenko.dds"
  expire = "1999.31.12"
	ideology = leninism
	traits = {
	}
}
create_country_leader = {
	name = "Vyacheslav Chornovil"
	desc = ""
	picture = "Chornovil.dds"
  expire = "1999.31.12"
	ideology = conservatism
	traits = {
	}
}
create_country_leader = {
	name = "Yuriy Shuhevych"
	desc = ""
	picture = "Shuhevych.dds"
  expire = "1999.31.12"
	ideology = rexism
	traits = {
	}
}
