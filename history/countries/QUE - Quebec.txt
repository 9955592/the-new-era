﻿capital = 466
set_research_slots = 3
#Starting tech
set_technology = {
}

set_politics = {
	ruling_party = social_democratic
	last_election = "1990.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	left_tolitarism = 6
	pragmatic_socialism = 4
	social_democratic = 49
	centrism = 4
	liberalism = 3
	conservatism = 20
	paternalism = 3
	authoritarism = 5
	nationalism = 6
}

