capital = 107

oob = "YUG_1936"

set_research_slots = 3



set_technology = {
	tech_support = 1
	tech_engineers = 1
	tech_mountaineers = 1
	early_fighter = 1
	gwtank = 1
	infantry_weapons = 1
	infantry_weapons1 = 1
	interwar_antitank = 1
	gw_artillery = 1
	early_destroyer = 1
	early_light_cruiser = 1
	early_submarine = 1
	superior_firepower = 1
}

add_political_power = 1198

set_convoys = 10
set_politics = {
	ruling_party = social_democratic
	last_election = "1990.1.1"
	election_frequency = 48
	elections_allowed = yes
}
set_popularities = {
	left_tolitarism = 0
	pragmatic_socialism = 6
	social_democratic = 29
	centrism = 17
	liberalism = 13
	conservatism = 22
	paternalism = 2
	authoritarism = 10
	nationalism = 1
}

set_stability = 0.6

create_country_leader = {
	name = "Bratko Kostic"
	desc = ""
	picture = "Bratko_Kostich.dds"
	expire = "1999.1.1"
	ideology = despotism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Slobodan Milishevich"
	desc = ""
	picture = "gfx/leaders/YUG/Slobodan_Milishevich.dds"
	expire = "1999.1.1"
	ideology = liberalism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Dobritsa Chosich"
	desc = ""
	picture = "gfx/leaders/YUG/Dobritsa_Chosich.dds"
	expire = "1999.1.1"
	ideology = liberalism
	traits = {
		#
	}
}

create_corps_commander = {
	name = "Kadievich Velko"
	gfx = Kadievich_Velko
	traits = { trickster  }
	skill = 1

	attack_skill = 2
	defense_skill = 3
	planning_skill = 3
	logistics_skill = 2
}

create_corps_commander = {
	name = "Ojdani Dragoljub"
	gfx = GFX_Ojdani_263__Dragoljub
	traits = { urban_assault_specialist }
	skill = 3

	attack_skill = 3
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 3
}

create_corps_commander = {
	name = "Nebojša Pavković"
	gfx = GFX_Pavkovi_263__Neboj_353_a
	traits = { urban_assault_specialist }
	skill = 3

	attack_skill = 3
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 3
}

create_navy_leader = {
	name = "Rust Franjo"
	gfx = GFX_Rust_Franjo
	traits = {  }
	skill = 2
}
