﻿capital = 686

oob = "PUE_1936"

set_technology = {
	infantry_weapons = 1
	tech_support = 1
	tech_recon = 1
}

set_politics = {
	ruling_party = liberalism
	last_election = "1988.11.8"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	left_tolitarism = 0
	pragmatic_socialism = 0
	social_democratic = 0
	centrism = 5
	liberalism = 49
	conservatism = 46
	paternalism = 0
	authoritarism = 0
	nationalism = 0
}