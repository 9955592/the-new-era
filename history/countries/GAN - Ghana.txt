﻿capital = 274
oob = «GAN_1936»
set_research_slots = 2
set_convoys = 10
add_political_power = 200
set_stability = 0.8
set_war_support = 0.1

set_politics = {
	ruling_party = liberalism
	last_election = "1990.1.1"
	election_frequency = 48
	elections_allowed = yes
}
set_popularities = {
	left_tolitarism = 0
	pragmatic_socialism = 0
	social_democratic = 25
	centrism = 25
	liberalism = 25
	conservatism = 25
	paternalism = 0
	authoritarism = 0
	nationalism = 0
}

create_country_leader = {
	name = "Hashim al-Atassi"
	desc = ""
	picture = "gfx/leaders/SYR/Portrait_Arabia_Generic_navy_1.dds"
	ideology = liberalism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Antun Saadeh"
	desc = ""
	picture = "gfx/leaders/SYR/Portrait_Arabia_Generic_land_3.dds"
	ideology = fascism_ideology
	traits = {
		#
	}
}

create_country_leader = {
	name = "Ghazi bin Faisal"
	desc = ""
	picture = "gfx/leaders/SYR/Portrait_Arabia_Generic_2.dds"
	ideology = despotism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Khalid Bakdash"
	desc = ""
	picture = "gfx/leaders/SYR/Portrait_Arabia_Generic_communism1.dds"
	ideology = leninism
	traits = {
		#
	}
}
