﻿capital = 305

oob = "ECU_1936"

# Starting tech
set_technology = {
	infantry_weapons = 1
	early_fighter = 1
}
set_country_flag = monroe_doctrine
set_war_support = 0.1
set_convoys = 5
set_politics = {
	ruling_party = liberalism
	last_election = "1990.1.1"
	election_frequency = 48
	elections_allowed = yes
}
set_popularities = {
	left_tolitarism = 0
	pragmatic_socialism = 0
	social_democratic = 25
	centrism = 25
	liberalism = 25
	conservatism = 25
	paternalism = 0
	authoritarism = 0
	nationalism = 0
}

create_country_leader = {
	name = "Federico Páez"
	desc = "POLITICS_FEDERICO_PAEZ_DESC"
	picture = "GFX_Portrait_ecuador_federico_paez"
	expire = "1965.1.1"
	ideology = socialism
	traits = {
		#
	}
}
