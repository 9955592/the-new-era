﻿capital = 439

oob = "RAJ_1936"


set_stability = 0.6
set_war_support = 0.1
# Starting tech
set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_support = 1
	tech_engineers = 1
	tech_recon = 1
	transport = 1
	trench_warfare = 1
	fleet_in_being = 1
	CAS1 = 1
}



set_convoys = 20

set_politics = {
	ruling_party = social_democratic
	last_election = "1990.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	left_tolitarism = 6
	pragmatic_socialism = 4
	social_democratic = 49
	centrism = 4
	liberalism = 3
	conservatism = 20
	paternalism = 3
	authoritarism = 5
	nationalism = 6
}
