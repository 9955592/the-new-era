﻿capital = 324

oob = "BHU_1936"

# Starting tech
set_technology = {
	infantry_weapons = 1
	tech_mountaineers = 1
}
set_war_support = 0.1
set_stability = 0.8
1939.1.1 = {

	add_political_power = 1198

	#generic focuses
	complete_national_focus = army_effort
	complete_national_focus = equipment_effort
	complete_national_focus = motorization_effort
	complete_national_focus = aviation_effort
	complete_national_focus = construction_effort_2
	complete_national_focus = production_effort_2
	complete_national_focus = infrastructure_effort
	complete_national_focus = industrial_effort
	complete_national_focus = construction_effort
	complete_national_focus = production_effort

	oob = "BHU_1939"
	set_technology = {
		infantry_weapons = 1
		tech_mountaineers = 1

		#doctrines
		mass_assault = 1
		pocket_defence = 1
		defence_in_depth = 1

		#electronics
		electronic_mechanical_engineering = 1
		radio = 1
		mechanical_computing = 1

		#industry
		basic_machine_tools = 1
		improved_machine_tools = 1
		advanced_machine_tools = 1
		synth_oil_experiments = 1
		construction1 = 1
		dispersed_industry = 1
	}
}
set_politics = {
	ruling_party = liberalism
	last_election = "1990.1.1"
	election_frequency = 48
	elections_allowed = yes
}
set_popularities = {
	left_tolitarism = 0
	pragmatic_socialism = 0
	social_democratic = 25
	centrism = 25
	liberalism = 25
	conservatism = 25
	paternalism = 0
	authoritarism = 0
	nationalism = 0
}
create_country_leader = {
	name = "Jigme Wangchuck"
	desc = "POLITICS_JIGME_WANGCHUCK_DESC"
	picture = "GFX_portrait_buthan_jigme_wangchuk"
	expire = "1965.1.1"
	ideology = despotism
	traits = {
		#
	}
}
