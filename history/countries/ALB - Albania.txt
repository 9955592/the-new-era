capital = 44

OOB = "ALB_1936"

set_technology = {
	infantry_weapons = 1
	tech_support = 1
	tech_recon = 1
}

set_politics = {
	ruling_party = liberalism
	last_election = "1990.1.1"
	election_frequency = 48
	elections_allowed = yes
}
set_popularities = {
	left_tolitarism = 0
	pragmatic_socialism = 0
	social_democratic = 25
	centrism = 25
	liberalism = 25
	conservatism = 25
	paternalism = 0
	authoritarism = 0
	nationalism = 0
}

set_convoys = 5

#Victor Emmanuel III of Italy after 39
create_country_leader = {
	name = "Ramiz Alia"
	desc = "POLITICS_KING_ZOG_DESC"
	picture = "Ramiz_Alia.dds"
	expire = "1965.1.1"
	ideology = despotism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Enver Hoxha"
	desc = "POLITICS_KING_ZOG_DESC"
	picture = "gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
	expire = "1965.1.1"
	ideology = stalinism
	traits = {
		staunch_stalinist
	}
}

create_field_marshal = {
	name = "Xhemal Aranitasi"
	portrait_path = "gfx/leaders/Europe/Portrait_Europe_Generic_land_3.dds"
	traits = { }
	skill = 2
	attack_skill = 1
    defense_skill = 3
    planning_skill = 2
    logistics_skill = 1
}
