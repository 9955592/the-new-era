state={
	id=941
	name="STATE_941"
	manpower = 1701855
	
	state_category = city
	
	resources={
		steel=14 # was: 112
	}


	history={
		owner = USA
		buildings = {
			infrastructure = 5
			industrial_complex = 1
		}
		add_core_of = USA
		victory_points = {
			7791 3 
		}

	}

	provinces={
		913 7791 8014 10657 10909  
	}
}
