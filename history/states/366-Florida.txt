
state={
	id=366
	name="STATE_366"
	manpower = 4665782
	
	state_category = large_city

	history={
		owner = USA
		buildings = {
			infrastructure = 7
			industrial_complex = 1
			air_base = 4
			1572 = {
				naval_base = 2
			}
		}
		add_core_of = USA
		victory_points = {
			1843 5 # Tallahassee
		}
		victory_points = {
			12381 2 # Jacksonville 
		}
		victory_points = {
			12439 5 # Orlando 
		}

	}

	provinces={
		1556 1572 1584 10407 12353 12381 12439
	}
	
}
