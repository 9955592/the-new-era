
state={
	id=200
	name="STATE_200" # Zaporozhe
	manpower = 2069000
	state_category = town

	history={
		owner = UKR
		victory_points = {
			9571 1
		}
		victory_points = {
			11405 10
		}
		victory_points = {
			11700 1
		}
		buildings = {
			infrastructure = 4
		}
		add_core_of = UKR
		1939.1.1 = {
			buildings = {
				industrial_complex = 1
			}
		}
	}

	provinces={
		429 588 3399 3767 6596 9571 9729 11405 11700
	}
}
