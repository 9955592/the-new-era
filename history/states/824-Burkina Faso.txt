state={
	id=824
	name="STATE_824"
	history={
		owner = BUF
		add_core_of = BUF
		victory_points = {
			10836 1
		}
		buildings = {
			infrastructure = 1
		}
	}
	provinces={
		2001 7950 7983 8071 10836 12851
	}
	manpower=2474083
	buildings_max_level_factor=1.000
	state_category=pastoral
}
