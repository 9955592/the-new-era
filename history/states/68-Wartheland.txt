
state={
	id=68
	name="STATE_68"
	manpower = 3250840
	resources={
		aluminium=15 # was: 24
	}

	state_category = city

	history={
		owner = POL
		victory_points = {
			6236 1
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 1
		}
		add_core_of = POL
	}

	provinces={
		444 537 3473 6236 9387 11478
	}
}
