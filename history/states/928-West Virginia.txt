state={
	id=928
	name="STATE_928"
	manpower = 1793477
	
	state_category = town

	resources={
		oil=6
	}

	history={
		owner = USA
		victory_points = {
			7588 5 
		}
		buildings = {
			infrastructure = 6
		}
		add_core_of = USA
	}

	provinces={
		801 3941 3975 4627 6850 6958 6971 7466 7588 7646 9923 10441 11888
	}
}
