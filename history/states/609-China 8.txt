
state={
	id=609
	name="STATE_609" # East Hebei
	manpower = 6400000

	state_category = city

	history={
		owner = PRC
		add_core_of = CHI
		add_core_of = PRC
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 1
		}

	}

	provinces={
		1027 1034 1438 1531 4074 4174 4469 10367 10480 10507 12300 12432
	}
}
