state={
	id=897
	name="STATE_897"
	resources={
		oil=5
	}
	history={
		owner = SOV
		add_core_of = ADY
		buildings = {
			infrastructure = 3
		}
		victory_points = {
			3717 1
		}
	}
	provinces={
		3717 3720 6738
	}
	manpower=436598
	buildings_max_level_factor=1.000
	state_category=rural
}
