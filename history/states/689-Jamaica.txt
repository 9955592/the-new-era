state={
	id=689
	name="STATE_689"
	manpower = 1075000
	
	state_category = town

	history={
		owner = JAM
		buildings = {
			infrastructure = 4
			12304 = {
				naval_base = 1
			}
		}
		victory_points = {
			12304 2
		}
		add_core_of = JAM
	}
	
	provinces={ 12304 12477 }
}
