
state={
	id=39
	name="STATE_39" # South Tyrol
	manpower = 2641650
	
	state_category = large_city
	
	resources={
		aluminium=14 # was: 22
	}

	history={
		owner = ITA
		victory_points = {
			969 3
		}
		victory_points = {
			6631 1 
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 1
		}
		add_core_of = ITA
	}

	provinces={
		969 6631 6661 6675 11568 11587 13229
	}
}
