
state={
	id=932
	name="STATE_932"
	manpower = 2667949
	
	state_category = large_city

	history={
		owner = USA
		victory_points = {
			6855 5 
		}
		buildings = {
			infrastructure = 5
			industrial_complex = 1
		}
		add_core_of = USA
	}

	provinces={
		827 971 3829 6855 7528 9760
	}
	
}
