state={
	id=960
	name="STATE_960"
	manpower = 445315
	
	state_category = rural

	history={
		owner = USA
		victory_points = {
			4464 1 
		}
		buildings = {
			infrastructure = 4
			4464 = {
				naval_base = 4
			}
		}
		add_core_of = USA
	}

	provinces={
		4464 
	}
	
}
