state={
	id=1023
	name="STATE_1023"
	manpower = 1666666

	state_category = town

	history={
		owner = YEM
		victory_points = {
			2029 1
		}
		buildings = {
			infrastructure = 3
			2029 = {
				naval_base = 1
			}
		}
		add_core_of = YEM
	}

	provinces={
		2029 4976 12894 13079
	}
}
