state={
	id=813
	name="STATE_813"
	history={
		owner = KOR
		buildings = {
			infrastructure = 7
			industrial_complex = 3
			4056 = {
				naval_base = 3
			}
		}
		add_core_of = KOR
		victory_points = {
			4056 20
		}
		victory_points = {
			4086 5
		}
		victory_points = {
			7121 5
		}
	}

	provinces={
		4056 4086 7121 12089
	}
	manpower=8856100
	buildings_max_level_factor=1.000
	state_category=metropolis
}
