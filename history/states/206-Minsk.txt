
state={
	id=206
	name="STATE_206"
	manpower =  3163163

	state_category = large_city

	history={
		owner = BLR
		victory_points = {
			11370 20
		}
		victory_points = {
			294 2
		}
		buildings = {
			infrastructure = 5
			industrial_complex = 3
			air_base = 8
			arms_factory = 2
		}
		add_core_of = BLR
	}

	provinces={
		216 294 3267 6220 6292 6371 11216 11322 11370
	}
}
