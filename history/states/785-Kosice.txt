state={
	id=785
	name="STATE_785"
	resources={
		steel=7.000
	}
	history={
		owner = CZE
		buildings = {
			infrastructure = 6
		}
		victory_points = {
			3581 1
		}
		victory_points = {
			6573 3
		}
		add_core_of = CZE
		add_core_of = SLO
	}
	provinces={
		555 3550 3565 3581 6573 6586 6604 9537
	}
	manpower=1584766
	buildings_max_level_factor=1.000
	state_category=city
}
