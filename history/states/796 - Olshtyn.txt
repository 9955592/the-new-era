state=
{
	id=796
	name="STATE_796"
	manpower = 3439675

	state_category = metropolis

	history=
	{
		owner = POL
    victory_points = { 3295 3 }
    victory_points = { 3380 4 }
    victory_points = { 11245 4 }
		buildings = {
			infrastructure = 8
			industrial_complex = 2
			air_base = 2
			dockyard = 1

		}
		add_core_of = POL

	}
	provinces=
	{
          266 3295 3351 3380 6347 6375 6402 9327 9346 9372 9398 11245 11386 }

}
