state={
	id=933
	name="STATE_933"
	manpower = 3000374
	
	state_category = large_city

	history={
		owner = USA
		victory_points = {
			4601 9 
		}
		victory_points = {
			831 5 
		}
		buildings = {
			infrastructure = 5
			industrial_complex = 1
		}
		add_core_of = USA
	}

	provinces={
		 831 4601 9775 11760 11776 11791 11890
	}
	
}