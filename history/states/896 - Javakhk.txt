state={
	id=896
	name="STATE_896"

	history={
		owner = GEO
		victory_points = {
			6669 1
		}
		buildings = {
			infrastructure = 4
            industrial_complex = 1
		}
        add_core_of = GEO
	}

	provinces={
		6669
	}
	manpower=69561
	buildings_max_level_factor=1.000
    state_category=rural
}
