state={
	id=88 # Krakow
	name="STATE_88"
	manpower = 2692800
	resources={
		steel=5 # was: 8
		oil=3 # was: 4
	}
	
	state_category = large_city

	history={
		owner = POL
		victory_points = {
			417 3		
		}
		victory_points = {
			442 5
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 1
			air_base = 4
		}
		add_core_of = POL
	}

	provinces={
		417 442 3410 6499 9412 11398 11413 11507
	}
}
