
state={
	id=869
	name="STATE_869"
	history={
		owner = AZR
		victory_points = {
			6997 1
		}
		buildings = {
			infrastructure = 4
			industrial_complex = 1
			air_base = 1

		}
		add_core_of = AZR
        add_core_of = ARM
	}

	provinces={
		6997
	}
	manpower=294320
	buildings_max_level_factor=1.000
	state_category=rural
}
