state={
	id=774
	name="STATE_774"
	history={
		owner = ITA
		victory_points = {
			6790 3 
		}
		victory_points = {
			6973 1 
		}
		victory_points = {
			9752 1 
		}
		buildings = {
			infrastructure = 8
			6973 = {
				naval_base = 10
			}
		}
		add_core_of = ITA
	}
	provinces={
		3773 3976 6790 6973 9752 11726
	}
	manpower=2270626
	buildings_max_level_factor=1.000
	state_category=large_city
}
