state={
	id=912
	name="STATE_912" # New Nampshire

	state_category = large_town

	history={
		owner = USA
		buildings = {
			infrastructure = 5
		}
		add_core_of = USA
		victory_points = {
			3712 1 # Manchester
		}
	}

	provinces={
		733 3712 3715 10481
	}
	manpower=1370000
	buildings_max_level_factor=1.000
}
