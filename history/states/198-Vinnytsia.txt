
state={
	id=198
	name="STATE_198"
	manpower = 1932000

	state_category = large_town

	history={
		owner = UKR
		buildings = {
			infrastructure = 4
			air_base = 3
                        industrial_complex = 1
		}
		victory_points = {
			9481 8
		}
		add_core_of = UKR
	}

	provinces={
		476 6455 6480 9423 9435 9481
	}
}
