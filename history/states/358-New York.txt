state={
	id=358
	name="STATE_358"
	resources={
		steel=13 # was: 24.000
	}
	
	state_category = megalopolis	

	history={
		owner = USA
		buildings = {
			infrastructure = 8
			industrial_complex = 3
			dockyard = 2
			air_base = 10
			3878 = {
				naval_base = 6

			}

		}
		add_core_of = USA
		victory_points = {
			3878 30.0 
		}
	}

	provinces={
		859 3878
	}
	manpower=11588061
	buildings_max_level_factor=1.000
}
