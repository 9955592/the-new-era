
state={
	id=161
	name="STATE_161"
	manpower = 1902401
	
	state_category = large_city
	
	resources={
		aluminium=9 # was: 16
	}

	history={
		owner = ITA
		victory_points = {
			782 3
		}
		victory_points = {
			6606 10
		}
		victory_points = {
			6793 3
		}
		buildings = {
			infrastructure = 7
			industrial_complex = 1
			air_base = 2
			782 = {
				naval_base = 2
			}
		}
		add_core_of = ITA
	}

	provinces={
		782 6606 6793 6985 11734 
	}
}
