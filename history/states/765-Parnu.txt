state={
	id=765
	name="STATE_765"
	history={
		owner = SOV
		victory_points = {
			719 5
		}
		victory_points = {
			9663 1
		}
		buildings = {
			infrastructure = 7
			719 = {
				naval_base = 3
				coastal_bunker = 1

			}
		}
		add_core_of = SOV
	}
	provinces={
		717 719 3698 3760 6721 6736 9663 9696 11668
	}
	manpower=2223066
	buildings_max_level_factor=1.000
	state_category=large_city
}
