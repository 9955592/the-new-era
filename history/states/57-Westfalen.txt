
state={
	id=57
	name="STATE_57"
	manpower = 8109333
	state_category = megalopolis

	history={
		owner = GER
		victory_points = {
			495 15 
		}
		victory_points = {
			11388 5 
		}
		victory_points = {
			11431 5 
		}
		buildings = {
			infrastructure = 8
			arms_factory = 3
			industrial_complex = 1
			air_base = 3

		}
		add_core_of = GER
	}

	provinces={
		405 495 3355 3398 6535 6622 9443 9509 11346 11388 11431 
	}
}
