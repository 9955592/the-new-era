state={
	id=936
	name="STATE_936"
	manpower = 7490675

	state_category = megalopolis

	history={
		owner = USA
		buildings = {
			infrastructure = 7
			industrial_complex = 8
		}
		add_core_of = USA
		victory_points = {
			9450 30
		}
	}

	provinces={
		9450
	}
}
