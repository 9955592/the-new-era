
state={
	id=63
	name="STATE_63"
	manpower = 1324417

	state_category = town

	history={
		owner = POL
		victory_points = {
			9361 1
		}
		victory_points = {
			11372 1
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 1
			air_base = 3
		}
		add_core_of = POL
	}

	provinces={
		6334 9334 9361 11343 11372
	}
}
