state={
	id=71
	name="STATE_71"
	history={
		owner = CZE
		buildings = {
			infrastructure = 4
			industrial_complex = 2
		}
		add_core_of = CZE
		add_core_of = SLO
		victory_points = {
			9551 1
		}
		victory_points = {
			11539 1
		}
	}
	provinces={
		581 3484 9551 11539 11554
	}
	manpower=1298900
	buildings_max_level_factor=1.000
	state_category=large_town
}
