state={
	id=951
	name="STATE_951"
	manpower = 1293150
	
	state_category = city

	history={
		owner = USA
		buildings = {
			infrastructure = 5
			industrial_complex = 1
		}
		add_core_of = USA
		victory_points = {
			860 3 
		}
	}

	provinces={
		492 860 4594 7640 10520 11509 11942 
	}
}
