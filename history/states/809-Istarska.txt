state={
	id=809
	name="STATE_809"
	history={
		owner = CRO
		add_core_of = YUG
		add_core_of = CRO
		buildings = {
			infrastructure = 2
			11735 = {
				naval_base = 5
			}
		}
	}
	provinces={
		591 3601 6611 11564 11735 13230 13232
	}
	manpower=354386
	buildings_max_level_factor=1.000
	state_category=town
}
