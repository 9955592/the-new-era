state=
{
	id=78
	name="STATE_78"
	manpower = 3063400

	state_category = city

	history=
	{
		owner = MOL
		buildings = {
			infrastructure = 4
			industrial_complex = 2
			air_base = 2
		}
        victory_points = { 6600 1 }
        victory_points = { 11686 5 }
		add_core_of = MOL
	}
	provinces=
	{
414 565 3577 3707 3724 6600 6743 11686 11705 	}
}
