
state={
	id=1008
	name="STATE_1008"
	manpower = 3364000

	state_category = large_city

	history={
		owner = HOL
		add_core_of = HOL
		victory_points = { 6241 1 }
		victory_points = { 9363 1 }
		buildings = {
			infrastructure = 7
			industrial_complex = 2
		} 
	}

	provinces={
		6241 6286 9363 9403 
	}
}
