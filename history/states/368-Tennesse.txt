
state={
	id=368
	name="STATE_368"
	manpower = 1712564
	
	state_category = city
	
	resources={
		steel=30 # was: 112
	}


	history={
		owner = USA
		buildings = {
			infrastructure = 5
			industrial_complex = 1
		}
		add_core_of = USA
		victory_points = {
			12501 3 
		}

	}

	provinces={
		1758 8083 10281 10824 12501 12670
	}
}
