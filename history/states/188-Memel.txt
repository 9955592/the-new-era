state=
{
	id=188
	name="STATE_188"
	manpower = 977041


	state_category = city

	history = {
		owner = LIT
		victory_points = {
			3288 1 
		}
		victory_points = {
			9483 1 
		}
		buildings = {
			infrastructure = 5
			3288 = {
				naval_base = 1
			}
			6314 = {
				naval_base = 1
			}
		}
		add_core_of = LIT

	}
	provinces={
		3288 3337 3354 6314 9483 9499 9731
	}
}
