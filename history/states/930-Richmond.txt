state={
	id=930
	name="STATE_930"
	manpower = 3149290
	
	state_category = large_city

	history={
		owner = USA
		buildings = {
			infrastructure = 6
			industrial_complex = 1
			dockyard = 1
			air_base = 10
        	}
		add_core_of = USA
		victory_points = {
			3823 20
		}
		victory_points = {
			873 5
		}
		victory_points = {
			865 5
		}
		victory_points = {
			10343 5
		}
	}

	provinces={
		865 873 951 3823 6846 10343 11873 
	}
}
