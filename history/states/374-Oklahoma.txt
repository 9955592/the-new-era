
state={
	id=374
	name="STATE_374"
	manpower = 2596039
	
	state_category = large_city
	
	resources={
		oil=40 # was: 100
		aluminium=20 # was: 32
	}


	history={
		owner = USA
		buildings = {
			infrastructure = 4
			industrial_complex = 1
			air_base = 2
		}
		add_core_of = USA
		victory_points = {
			1806 1 
		}
	}

	provinces={
		1618 1806 2043 5103 7762 7904 10798 11802 12624 
	}
}
