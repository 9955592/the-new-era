state={
	id=955
	name="STATE_955"
	manpower = 10378519
	
	state_category = megalopolis	

	history={
		owner = USA
		buildings = {
			infrastructure = 8
			industrial_complex = 3
			air_base = 8
			1843 = {
				naval_base = 5
			}
			7388 = {
				naval_base = 3
			}
		}
		add_core_of = USA
		victory_points = {
			7388 10 # Tampa 
		}
		victory_points = {
			1843 10 # Miami 
		}

	}

	provinces={
		866 1843 1913 7388 9834 10489 12752 
	}
	
}
