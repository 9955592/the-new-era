
state={
	id=608
	name="STATE_608"
	manpower = 45648400

	state_category = megalopolis

	history={
		owner = PRC
		add_core_of = CHI
		add_core_of = PRC
		buildings = {
			infrastructure = 5
			industrial_complex = 2
			air_base = 3
			10068 = {
				naval_base = 2
			}
		}
		victory_points = {
			9843 30
		}
		victory_points = {
			10068 5
		}
		victory_points = {
			1052 5
		}
	}

	provinces={
		881 900 952 1052 3955 6904 6969 9768 9776 9843 10068 11761 11822 12043
	}
}
