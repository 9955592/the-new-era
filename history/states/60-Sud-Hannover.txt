
state={
	id=60
	name="STATE_60"
	manpower = 4005214
	resources={
		steel=13 # was: 20
		chromium = 3 # was: 4
		tungsten = 3 # was: 4
	}
	
	state_category = large_city

	history={
		owner = GER
		victory_points = { 6421 1 }
		victory_points = { 6524 5 }
		victory_points = { 11417 3 }
		buildings = {
			infrastructure = 8
			industrial_complex = 1
		}
		add_core_of = GER
		1939.1.1 = {
			buildings = {
				synthetic_refinery = 5
				arms_factory = 2
			}
		}
	}

	provinces={
		425 482 538 3474 3500 3561 6421 6524 6582 6594 9497 11417 13116 
	}
}
