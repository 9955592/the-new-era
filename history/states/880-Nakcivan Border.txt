state={
	id=880
	name="STATE_880"
	history={
		owner = AZR
		victory_points = {
			13207 1
		}
		buildings = {
			infrastructure = 3

		}
		add_core_of = AZR
		add_core_of = ARM
	}

	provinces={
		13207
	}
	manpower=12128
	buildings_max_level_factor=1.000
	state_category=rural
}
