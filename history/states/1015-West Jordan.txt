
state={
	id=1015
	name="STATE_1015"
	manpower = 750521

	state_category = town

	history={
		owner = JOR
		buildings = {
			infrastructure = 3
			industrial_complex = 1
		}
		add_core_of = JOR
	}

	provinces={
		1544 4440 4562 4574 4591 4603 7001 11976
	}
}
