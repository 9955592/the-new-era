state={
	id=102
	name="STATE_102"
	history={
		owner = SLV
		buildings = {
			infrastructure = 7
		}
		victory_points = {
			9627 3
		}
		add_core_of = SLV
	}
	provinces={
		3631 6650 9596 9627
	}
	manpower=1200000
	buildings_max_level_factor=1.000
	state_category=town
}
