state=
{
	id=872
	name="STATE_872"
	manpower = 264364


	state_category = rural

	history=
	{
		owner = BRU
		victory_points = {
			1208 1
		}
		buildings = {
			infrastructure = 3
			7387 = {
				naval_base = 2
			}
		}
		add_core_of = BRU
	}

	provinces={
		7387 7371
	}
}
