
state={
	id=962
	name="STATE_962"
	manpower = 2440796
	
	state_category = large_city
	
	resources={
		oil=44 # was: 66
	}

	history={
		owner = USA
		buildings = {
			infrastructure = 6
			industrial_complex = 1
			7552 = {
				naval_base = 5
			}
		}
		add_core_of = USA
		victory_points = {
			7552 10 
		}

	}

	provinces={
		1453 4535 7524 7552 12398
	}
}
