state={
	id=73
	name="STATE_73"
	manpower = 1452000

	state_category = rural

	history={
	victory_points = {
		9563 3
	}
	victory_points = {
		11691 5
	}
		owner = UKR
		buildings = {
			infrastructure = 3
		}
		add_core_of = UKR
	}

	provinces={
		3548 6571 9563 11536 11691
	}
}
