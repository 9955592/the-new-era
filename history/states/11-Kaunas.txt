state={
	id=11
	name="STATE_11"
	history={
		owner = LIT
		victory_points = {
			6296 5
		}
		victory_points = {
			9438 1
		}
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 3
		}
		add_core_of = LIT
	}
	provinces={
		530 6229 6296 6360 6376 9246 9357 9374 9438
	}
	manpower=970626
	buildings_max_level_factor=1.000
	state_category=city
}
