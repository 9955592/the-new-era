state={
	id=1006
	name="STATE_1006" # Ghent
	manpower = 2447700
	
	state_category = large_city

	history={
		owner = BEL
		victory_points = { 6446 15 }
		victory_points = { 6560 5 }
		buildings = {
			infrastructure = 9
			industrial_complex = 2
			6560 = {
				naval_base = 3
			}
		}
		
		add_extra_state_shared_building_slots = 2
		
		add_core_of = BEL
	}

	provinces={
		3576 6446 6560 
	}
}
