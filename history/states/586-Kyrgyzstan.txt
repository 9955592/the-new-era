
state={
	id=586
	name="STATE_586" #Alma Alta
	resources={
		steel=48
		chromium=44
		tungsten=14
		aluminium=32
	}


	state_category = city

	history={
		owner = KGZ
		buildings = {
			infrastructure = 2
			industrial_complex = 2

		}
		victory_points = {
			1591 3
		}
 
		1939.1.1 = {
			buildings = {
				industrial_complex = 3
			}
		}
		add_core_of = 586
	}

	provinces={
		1591 1825 4224
	}
	manpower=2563404
}
