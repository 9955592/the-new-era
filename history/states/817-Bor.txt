state={
	id=817
	name="STATE_817"
	history={
		owner = PRK
		buildings = {
			infrastructure = 4
		}
		add_core_of = PRK
		victory_points = {
			4107 1
		}
		victory_points = {
			6928 5
		}
	}
	provinces={
		848 1003 4004 4107 6928 7070 7155 7171 9981 10065 10083 11770 11915 12040
	}
	manpower=8511000
	buildings_max_level_factor=1.000
	state_category=large_town
}
