state={
	id=261
	name="STATE_261"
	resources={
		steel=28 # was: 48.000
		aluminium=9 # was: 16.000
	}
	
	state_category = metropolis

	history={
		owner = USA
		buildings = {
			infrastructure = 7
			arms_factory = 1
			industrial_complex = 9
		}
		add_core_of = USA
		victory_points = {
			6874 5 
		}
		victory_points = {
			6890 10 
		}
	}

	provinces={
		659 882 3685 3853 3872 4442 6874 6890 9808 9825 13154 
	}
	manpower=4544640
	buildings_max_level_factor=1.000
}
