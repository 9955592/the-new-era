state={
	id=820
	name="STATE_820"
	history={
		owner = SYR
		add_core_of = SYR
		victory_points = {
			1578 1
		}
		victory_points = {
			1634 2
		}
		buildings = {
			infrastructure = 2
		}
	}
	provinces={
		901 1549 1578 1606 1634 6883 10882
	}
	manpower=3805255
	buildings_max_level_factor=1.000
	state_category=town
}
