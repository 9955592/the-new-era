state={
	id=871
	name="STATE_871"


	state_category = large_town
        manpower = 2893200

	resources={
		rubber=848
		steel=32
		tungsten=308
		aluminium = 25
	}

	history={
		owner = MAL
		victory_points = {
			7329 1
		}
		victory_points = {
			10297 3
		}
		buildings = {
			infrastructure = 3
			industrial_complex = 1
			air_base = 6
			7329 = {
				naval_base = 1
			}
			10297 = {
				naval_base = 5
			}
		}
		add_core_of = MAL
	}
	provinces={
		1291 1348 1364 1376 1392 4310 4355 4367 4384 4412 4424 7329 7342 7399 7427 10227 10297 10313 12113 12144 12199 12215 12255 12271
	}
	manpower=303020
	buildings_max_level_factor=1.000
}
