
state={
	id=67
	name="STATE_67"
	manpower = 1802591
	resources={
		steel=19 # was: 60
		aluminium=6 # was: 20
	}

	state_category = city

	history={
		victory_points = {
			3510 1
		}
		victory_points = {
			6462 1
		}
		victory_points = {
			11467 5
		}
		owner = POL
		buildings = {
			infrastructure = 7
		}
		add_core_of = POL
	}

	provinces={
		479 506 3283 3510 6462 9470 9511 11467 11558
	}
}
