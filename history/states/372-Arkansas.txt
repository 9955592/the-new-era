
state={
	id=372
	name="STATE_372"
	manpower = 1184286
	
	state_category = large_town
	
	resources={
		aluminium=14 # was: 20
	}


	history={
		owner = USA
		buildings = {
			infrastructure = 4
			industrial_complex = 1
		}
		add_core_of = USA
		victory_points = {
			12489 3 
		}

	}

	provinces={
		1503 7485 7543 10355 12458 12489 12778 
	}
}
