state={
	id=997
	name="STATE_997"
	provinces={
		3796 3819 3952 6842 9610
	}
	manpower=193681
	buildings_max_level_factor=6.000

	state_Category = city

	history = {
	    owner = BUL
	    victory_points = {
	        3796 20
	    }
	    buildings = {
	        infrastructure = 7
	        industrial_complex = 3
	        arms_factory = 1
	    }
	    add_core_of = BUL
	}
}
