state={
	id=885
	name="STATE_885"
	history={
		owner = NCY
		victory_points = { 11984 5 }
		buildings = {
			infrastructure = 4
			air_base = 1
			11984 = {
				naval_base = 2
			}
		}
		add_core_of = NCY
		add_core_of = CYP
	}

	provinces={
		11984
	}
	manpower=607133
	buildings_max_level_factor=1.000
	state_category=rural
}
