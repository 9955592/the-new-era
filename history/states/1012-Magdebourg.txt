state={
	id=1012
	name="STATE_1012"

	state_category = large_town
	history={
		owner = GER
		victory_points = {
			3522 10
		}
		victory_points = {
			9375 1
		}
		buildings = {
			infrastructure = 9
			arms_factory = 1
			industrial_complex = 1
			air_base = 3

		}
		add_core_of = GER
	}

	provinces={
		3522 6487 9238 9264 9375 11359 11468
	}
	manpower=1586854
	buildings_max_level_factor=1.000
}
