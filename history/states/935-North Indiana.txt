
state={
	id=935
	name="STATE_935"
	manpower = 1908000

	state_category = large_city

	history={
		owner = USA
		buildings = {
			infrastructure = 5
			industrial_complex = 1
		}
		add_core_of = USA
		victory_points = {
			870 1 # Fort-Wayne
		}

	}

	provinces={
		870 4437 6713 9653 9654 11637
	}
}
