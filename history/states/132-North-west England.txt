state= {
	id=132
	name="STATE_132" # Lancashire
	manpower = 3770454
	
	state_category = metropolis
	

	history= {
		owner = ENG
		victory_points = {
			6384 30 
		}
		buildings = {
			infrastructure = 8
			arms_factory = 1
			industrial_complex = 1
			dockyard = 3
			air_base = 3
			6384 = {
				naval_base = 5
			}
		}
		add_core_of = ENG
	}
	provinces= {
		3205 6335 6384
 	}
}
