
state={
	id=377
	name="STATE_377"
	manpower = 3451080
	
	state_category = large_city
	
	resources={
		steel=20 # was: 40
		oil=5 # was: 10
		aluminium=8 # was: 16
	}

	history={
		owner = USA
		buildings = {
			infrastructure = 4
			industrial_complex = 1
			air_base = 2
		}
		add_core_of = USA
		victory_points = {
			853 3 
		}

	}

	provinces={
		853 1793 4796 6897 7747 7821 8115 10573 11814 12765 
	}
}
