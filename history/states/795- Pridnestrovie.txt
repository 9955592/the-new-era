state=
{
	id=795
	name="STATE_795"
	manpower = 525665

	state_category = town

	history=
	{
		owner = PMR
                victory_points = { 741 1 }
		buildings = {
			infrastructure = 4
			industrial_complex = 1
			air_base = 2
		}
		add_core_of = PMR

	}
	provinces=
	{
          741 754 9576 }

}
