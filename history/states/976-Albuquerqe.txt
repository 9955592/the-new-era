
state={
	id=976
	name="STATE_976"
	manpower = 1277610
	
	state_category = city
	
	resources={
		oil=14 # was: 20
		aluminium=30 # was: 52
	}


	history={
		owner = USA
		buildings = {
			infrastructure = 4
			industrial_complex = 1
		}
		add_core_of = USA
		victory_points = {
			4975 3 
		}
	}

	provinces={
		3867 3883 4975 8070 12791 
	}
	
}
