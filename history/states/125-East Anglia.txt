
state={
	id=125
	name="STATE_125"
	manpower = 5363174
	
	state_category = metropolis

	history={
		owner = ENG
		victory_points = { 322 5 }
		victory_points = { 221 5 }
		victory_points = { 11374 5 }
		victory_points = { 11221 5 }
		victory_points = { 9239 5 }
		buildings = {
			infrastructure = 7
			arms_factory = 1
			industrial_complex = 3
			anti_air_building = 2
			radar_station = 1
			air_base = 10
			322 = {
				naval_base = 3
			}
			11374 = {
				naval_base = 10
			}
		}
		add_core_of = ENG
	}

	provinces={
		221 271 322 3287 9239 11221 11253 11374 
	}
}
