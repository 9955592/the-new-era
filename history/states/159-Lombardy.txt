
state={
	id=159
	name="STATE_159"
	manpower = 6897704
	state_category = megalopolis

	history={
		owner = ITA
		victory_points = {
			3780 30 
		}
		buildings = {
			infrastructure = 8
			arms_factory = 7
			industrial_complex = 3
			air_base = 10

		}
		add_core_of = ITA
	}

	provinces={
		607 3780 9584 
	}
}
