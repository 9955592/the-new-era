
state={
	id=454
	name="STATE_454" #Palestine
	manpower = 7641599

	state_category = metropolis

	history={
		owner = ISR
		buildings = {
			infrastructure = 7
			industrial_complex = 5
			air_base = 2
			4206 = {
				naval_base = 3
			}
		}
		victory_points = {
			1086 10
		}
		victory_points = {
			4206 10
		}
		add_core_of = PAL
		add_core_of = ISR
	}

	provinces={
		1086 4088 4206
	}
}
