
add_namespace = low_surrender_events
# First New Decision
country_event = {
	id = low_surrender_events.1
	title = low_surrender_events.1.t
	desc = low_surrender_events.1.d
	picture = GFX_report_event_generic_read_write
	
	trigger = {
		NOT = { has_idea = we_need_to_surrender_idea }
	}
	hidden = yes
	mean_time_to_happen = {
		days = 3
	}
	option = {
		name = low_surrender_events.1.a
		add_ideas = we_need_to_surrender_idea
	}
}