add_namespace = belgium

#PS Forces Seats
country_event = {
	id = belgium.1
	title = belgium.1.t
	desc = belgium.1.d
	picture = GFX_report_event_generic_sign_treaty1

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = belgium.1.a
		add_popularity = {
			ideology = pragmatic_socialism
			popularity = 0.10
		}
	}
}

#PS Wins Referendum
country_event = {
	id = belgium.2
	title = belgium.2.t
	desc = belgium.2.d
	picture = GFX_report_event_generic_sign_treaty1

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = belgium.2.a
	}
}

#SP Leader
country_event = {
	id = belgium.3
	title = belgium.3.t
	desc = belgium.3.d
	picture = GFX_report_event_generic_sign_treaty1

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = belgium.3.a
		set_politics = {
			ruling_party = social_democratic
			elections_allowed = yes
		}
		add_popularity = {
			ideology = social_democratic
			popularity = 0.1
		}
	}
}

