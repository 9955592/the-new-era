add_namespace = can

##Event: Assassination of Don Clark
news_event = {
	id = can.01
	title = can.01.title
	desc = can.01.desc
	picture = GFX_report_event_soviet_purge_speech
	fire_only_once = yes
	major = yes
	mean_time_to_happen = {
		days = 5
	}
	trigger = {
		tag = CAN
		date > 1991.1.1
	}
	option = {
		name = can.01.a
		ai_chance = { factor = 1}	
		add_stability = -0.1
		add_war_support = 0.2
	}
	option = {
		name = can.01.b
		ai_chance = { factor = 1}
		add_stability = 0.2
		add_war_sipport = -0.1
	}
}
##Event: Assassination of Al Gore!
news_event = {
	id = can.02
	title = can.02.title
	desc = can.02.desc
	picture = GFX_report_event_romania_poland_visit
	is_triggered_only = yes
	fire_only_once = yes
	option = {
		name = can.02.a
		ai_chance = { factor = 1}
		add_war_support = 0.15
	}
}
##Event: Political Revenge
country_event = {
	id = can.03
	title = can.03.title
	desc = can.03.desc
	picture = GFX_report_event_asian_politicians
	is_triggered_only = yes
	fire_only_once = yes
	option = {
		name = can.03.a
		ai_chance = { factor = 1}
		country_event = { id = can.02 }
	}
	option = {
		name = can.03.b
		ai_chance = { factor = 1}
		add_stability = 0.1
	}
}
##Event: Export German Nationalists
country_event = {
	id = can.04
	title = can.04.title
	desc = can.04.desc
	picture = GFX_report_event_french_liberation
	is_triggered_only = yes
	fire_only_once = yes
	option = {
		name = can.04.a
		ai_chance = { factor = 1}
	}
	option = {
		name = can.04.b
		ai_chance = { factor = 1}
	}
}
##Event: Export Japanese Nationalist
country_event = {
	id = can.05
	title = can.05.title
	desc = can.05.desc
	picture = GFX_report_event_chinese_officers
	is_triggered_only = yes
	fire_only_once = yes
	option = {
		name = can.05.a
		ai_chance = { factor = 1}
	}
	option = {
		name = can.05.b
		ai_chance = { factor = 1}
	}
}
##Event: Canadian Effort
country_event = {
	id = can.06
	title = can.06.title
	desc = can.06.desc
	picture = GFX_report_event_spr_anarchists2
	is_triggered_only = yes
	fire_only_once = yes
	option = {
		name = can.06.a
		ai_chance = { factor = 1}
	}
	option = {
		name = can.06.b
		ai_chance = { factor = 1}
	}
}
##Event: Canada ask to Purchase Alaska
country_event = {
	id = can.07
	title = can.07.title
	desc = can.07.desc
	picture = GFX_report_event_usa_election_generic
	is_triggered_only = yes
	fire_only_once = yes
	option = {
		name = can.07.a
		ai_chance = { factor = 1}
	}
	option = {
		name = can.07.b
		ai_chance = { factor = 1}
	}
}
##Event: Canada's Royal Family
country_event = {
	id = can.08
	title = can.08.title
	desc = can.08.desc
	picture = GFX_report_event_aid_pact
	is_triggered_only = yes
	fire_only_once = yes
	option = {
		name = can.08.a
		ai_chance = { factor = 1}
	}
	option = {
		name = can.08.b
		ai_chance = { factor = 1}
	}
}
##Event: Vote for Alaska
country_event = {
	id = can.09
	title = can.09.title
	desc = can.09.desc
	picture = GFX_report_event_soviet_purge_trial
	is_triggered_only = yes
	fire_only_once = yes
	option = {
		name = can.09.a
		ai_chance = { factor = 1}
	}
	option = {
		name = can.09.b
		ai_chance = { factor = 1}
	}
}
##Event: Canada seeks political allies
country_event = {
	id = can.010
	title = can.010.title
	desc = can.010.desc
	picture = GFX_report_event_chinese_japanese_handshake
	is_triggered_only = yes
	fire_only_once = yes
	option = {
		name = can.010.a
		ai_chance = { factor = 1}
	}
	option = {
		name = can.010.b
		ai_chance = { factor = 1}
	}
}
